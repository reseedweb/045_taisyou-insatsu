			<section id="top-main" class="contain clearfix">
				<h2 id="lineup" class="top-title"><span>形状</span>から<span>化粧品パッケージ</span>を選ぶ</h2>
				<div class="top-main-group top-main-col230 clearfix"><!-- begin top-main-group -->
					<div class="top-main-row clearfix"><!--top-main-row -->
						<div class="top-main-col">	
							<a href="<?php bloginfo('url'); ?>/sack">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/top/top_main_img1.jpg" alt="<?php bloginfo('name');?>" />
								</div>
								<h3 class="title">サック箱</h3>
								<div class="text">
									<p>箱の上下に差し込み部用の蓋がついている標準的なパッケージ形状です。口別名「キャラメル箱」とも呼ばれています。</p>
								</div>  								
							</a>
						</div><!-- end top-main-col -->
						
						<div class="top-main-col">								
							<a href="<?php bloginfo('url'); ?>/onetouch">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/top/top_main_img2.jpg" alt="<?php bloginfo('name');?>" />
								</div>   
								<h3 class="title">ワンタッチ底</h3>
								<div class="text">
									<p>底の一部が接着処理されており、折りたたんだ状態から開くとすぐに底が組まれるため 簡単に商品を入れるパッケージができあがります。</p>
								</div>            							
							</a>
						</div><!-- end top-main-col -->
						
						<div class="top-main-col">								
							<a href="<?php bloginfo('url'); ?>/jigokuzoko">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/top/top_main_img3.jpg" alt="<?php bloginfo('name');?>" />
								</div>
								<h3 class="title">地獄底</h3>
								<div class="text">
									<p>箱の底部分を組み合わせることによって、底が抜けないように作られています。他のパッケージ形状よりも非常に強度があり、安定している形状です。</p>
								</div>            
							</a>
						</div><!-- end top-main-col -->
							
						<div class="top-main-col">								
							<a href="<?php bloginfo('url'); ?>/piro">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/top/top_main_img4.jpg" alt="<?php bloginfo('name');?>" />
								</div>   
								<h3 class="title">ピローケース</h3>
								<div class="text">
									<p>化粧品以外にも洋・和菓子、衣料品、アクセサリーなど様々なパッケージに用いられるのがこのピローケースです。</p>
								</div>  
							</a>
						</div><!-- end top-main-col -->								
					</div><!-- end top-main-row -->
						
					<div class="top-main-row clearfix"><!--top-main-row -->
						<div class="top-main-col">								
							<a href="<?php bloginfo('url'); ?>/kanban">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/top/top_main_img5.jpg" alt="<?php bloginfo('name');?>" />
								</div>
								<h3 class="title">看板付き</h3>
								<div class="text">
									<p>店舗販売などの陳列スペース用として使われています。ヘッダー付きとも言われ、アイキャッチとして使用されることが多いタイプです。</p>
								</div>           
							</a>
						</div><!-- end top-main-col -->
							
						<div class="top-main-col">								
							<a href="<?php bloginfo('url'); ?>/madohari">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/top/top_main_img6.jpg" alt="<?php bloginfo('name');?>" />
								</div>   
								<h3 class="title">窓貼り</h3>
								<div class="text">
									<p>フィルム貼りを通し、中の商品を見せたい時に用いられます。商品を直接触られることなく見せることが可能なので安心・安全です。</p>
								</div>            
							</a>
						</div><!-- end top-main-col -->
							
						<div class="top-main-col">								
							<a href="<?php bloginfo('url'); ?>/daishi">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/top/top_main_img7.jpg" alt="<?php bloginfo('name');?>" />
								</div>
								<h3 class="title">台紙</h3>
								<div class="text">
									<p>シェルパックブリスター・スライドブリスター・クリアケースなどに、アイキャッチや責任表示が書かれた台紙をセットして使用します。</p>
								</div>            
							</a>
						</div><!-- end top-main-col -->
							
						<div class="top-main-col">								
							<a href="<?php bloginfo('url'); ?>/special">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/top/top_main_img8.jpg" alt="<?php bloginfo('name');?>" />
								</div>   
								<h3 class="title">特殊形状</h3>
								<div class="text">
									<p>四角い箱以外にも多角形パッケージやブック型パッケージなど、企画に合わせた形状もご提案させていただきます。</p>
								</div>            
							</a>
						</div><!-- end top-main-col -->														
					</div><!-- end top-main-row -->						
				</div><!-- end top-main-group -->				
			</section><!-- end top-main -->
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">ワンタッチ箱</h3>
		<table class="price-table">
			<tr>
				<th>ロット数</th>
				<th>１色単価例</th>
				<th>4色単価例</th>
				<th>6色単価例</th>
			</tr>
			<tr>
				<th>1,000</th>
				<td>82円</td>
				<td>92円</td>				
				<td>142円</td>
			</tr>
			<tr>
				<th>5,000</th>
				<td>19円</td>
				<td>26円</td>				
				<td>37円</td>
			</tr>
			<tr>
				<th>10,000</th>
				<td>12円</td>
				<td>19円</td>				
				<td>21円</td>
			</tr>
		</table><!-- .price-table -->
	</div><!-- end primary-row -->	
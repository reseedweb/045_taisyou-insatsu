<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="subpages-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/madohari_top_img.jpg" alt="窓貼り" />
			<span>窓貼り</span>
		</h2> 
		
		<div class="sack-top-left clearfix">
			<div id="mainalbum">										
				<span><img src="<?php bloginfo('template_url'); ?>/img/content/madohari_slide_img1.jpg" alt="窓貼り" class="mainImage" /></span>
			</div>
			<div id="subalbum">
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/madohari_slide_img1.jpg" alt="窓貼り" class="thumb" name="mainImage" /></p>
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/madohari_slide_img2.jpg" alt="窓貼り" class="thumb" name="mainImage" /></p>
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/madohari_slide_img3.jpg" alt="窓貼り" class="thumb" name="mainImage" /></p>
			</div>
		</div><!-- .sack-top-left -->		
		
		<div class="sack-top-right ln18em clearfix">
			<p>フィルム貼りを通し、中の商品を見せたい時に用いられます。商品を直接触られることなく見せることが可能なので安心・安全です。</p>
		</div>	
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="sack-tabs htabs clearfix">
			<a href="#guide" class="guide">仕様・制作ガイド<i class="fa fa-arrow-down"></i></a>
			<a href="#price" class="price">参考価格例<i class="fa fa-arrow-down"></i></a>
			<a href="#estimate" class="estimate">お見積りフォーム<i class="fa fa-arrow-down"></i></a>										
		</div>	
	</div><!-- end primary-row -->
	
	<div id="guide" class="primary-row clearfix">
		<?php get_template_part('part','guide2') ;?>	
	</div><!-- #guide -->
	
	<div id="price" class="primary-row clearfix">
		<?php get_template_part('part','price06') ;?>	
	</div><!-- #price -->
	
	<div id="estimate" class="primary-row clearfix">		
		<?php get_template_part('part','estimation'); ?>	
	</div><!-- #estimate -->
	
	<?php get_template_part('part','flow_contact');?>
	
<?php get_footer(); ?>
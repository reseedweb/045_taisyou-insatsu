	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">ピローケース</h3>
		<table class="price-table">
			<tr>
				<th>ロット数</th>
				<th>１色単価例</th>
				<th>4色単価例</th>
				<th>6色単価例</th>
			</tr>
			<tr>
				<th>1,000</th>
				<td>80円</td>
				<td>90円</td>				
				<td>130円</td>
			</tr>
			<tr>
				<th>5,000</th>
				<td>17円</td>
				<td>24円</td>				
				<td>30円</td>
			</tr>
			<tr>
				<th>10,000</th>
				<td>12円</td>
				<td>17円</td>				
				<td>19円</td>
			</tr>
		</table><!-- .price-table -->
	</div><!-- end primary-row -->	
                    </main><!-- end primary -->
                    <aside class="sidebar"><!-- begin sidebar -->
                        <?php if(is_page('blog') || is_category() || is_single()) : ?>
                            <?php
                            $queried_object = get_queried_object();                                
                            $sidebar_part = 'blog';
                            if(is_tax() || is_archive()){                                    
                                $sidebar_part = '';
                            }                               

                            if($queried_object->post_type != 'post' && $queried_object->post_type != 'page'){
                                $sidebar_part = '';
                            }   
                                    
                            if($queried_object->taxonomy == 'category'){                                    
                                $sidebar_part = 'blog';
                            }                 
                            ?>
                            <?php get_template_part('sidebar',$sidebar_part); ?>  
                        <?php else: ?>
                            <?php get_template_part('sidebar'); ?>  
                        <?php endif; ?>                                    
                    </aside>                            
                </div><!-- end wrapper -->            
            </section><!-- end content -->

            <footer><!-- begin footer -->
				<div id="footer-info1">
					<p class="contain clearfix">化粧品パッケージ・印刷・製作なら化粧品パッケージ製作.com</p>
				</div>
				
				<div id="footer-info2">
					<div class="contain clearfix">
						<div class="footer-logo">
							<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/img/common/footer_logo.jpg" alt="<?php bloginfo('name');?>" /></a>
						</div>
						<div class="footer-tel">
							<img src="<?php bloginfo('template_url'); ?>/img/common/footer_tel.jpg" alt="<?php bloginfo('name');?>" />
						</div>
						<div class="footer-con">
							<a href="<?php bloginfo('url'); ?>/contact"><img src="<?php bloginfo('template_url'); ?>/img/common/footer_con.jpg" alt="<?php bloginfo('name');?>" /></a>
						</div>
					</div>					
				</div>
				
				<?php get_template_part('part','footernavi'); ?>
                
                <div id="footer-copyright"><!-- begin wrapper -->
					<p class="contain clearfix">Copyright©2015 化粧品パッケージ製作.com All Rights Reserved.</p>
                </div>         
            </footer><!-- end footer -->            

        </div><!-- end wrapper -->
        <?php wp_footer(); ?>              
    </body>
</html>
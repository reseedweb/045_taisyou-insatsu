$(document).ready(function() {		
	/*----- js topnavi current -----*/
	$("#top .top-navi li a").each(function(){
        if ($(this).attr("href") == window.location.href){
			$(this).addClass("top-navi-current");
        }
	});
	
	/*----- js sideMenu current -----*/
	$("#content .sideMenu li a").each(function(){
        if ($(this).attr("href") == window.location.href){
			$(this).addClass("sideMenu-current");
        }
	});

	/*----- js sideNav current -----*/
	$("#content .sideNav li a").each(function(){
        if ($(this).attr("href") == window.location.href){
			$(this).addClass("sideNav-current");
        }
	});
	
	/*----- js type-tabs-detail current -----*/
	$("footer #footer-info3 .footer-col li a").each(function(){
       	if ($(this).attr("href") == window.location.href){
		  $(this).addClass('footernavi-current');
		}
	});
	
	$('.htabs a').htabs();	
	
	$( "#tabs" ).tabs();
	
	$("#tabs").bind('tabsselect', function(event, ui) {
        window.location.href=ui.tab;
    });
});

jQuery(function(){
	$(this).tgImageChange({
	selectorThumb : '.thumb', // ←★サムネイル画像セレクタ(ドットは必須)
	fadeOutTime : 50, // ←★カーソルON時のアニメーション時間
	fadeInTime : 200, // ←★カーソルOFF時のアニメーション時間
	thumbCssBorder : '2px solid #ff5a71', // ←★カーソルON時のサムネイル枠CSS
	});
}); 

				<div id="footer-info3">
					<div class="contain clearfix">
						<ul class="footer-contact">
							<li>大昇印刷株口式会社</li>
							<li>〒578-0983 大阪府東大阪市吉田下島12番15号</li>
							<li>TEL：072-962-5301（代表）</li>
							<li>FAX：072-964-2621</li>								
						</ul>
						
						<ul class="footer-col">
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>トップページ</a></li>
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>サック箱</a></li>
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>ワンタッチ底</a></li>
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>地獄底</a></li>
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>ピローケース</a></li>
						</ul>
						
						<ul class="footer-col">
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>看板付き</a></li>
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>窓張り</a></li>
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>台紙</a></li>
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>特殊形状</a></li>
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>参考価格例</a></li>
						</ul>
						
						<ul class="footer-col">
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>見積りを依頼する</a></li>
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>はじめての方へ</a></li>
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>ご注文の流れ</a></li>
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>よくあるご質問</a></li>
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>データ入稿について</a></li>
						</ul>
						
						<ul class="footer-col">
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>制作実績</a></li>
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>工場紹介</a></li>
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>会社概要</a></li>
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>プライバシーポリシー</a></li>
							<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-arrow-right"></i>お問い合わせ</a></li>
						</ul>
							
					</div>                    
                </div>
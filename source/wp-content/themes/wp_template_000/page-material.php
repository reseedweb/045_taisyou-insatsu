<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="subpages-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/material_top_img.jpg" alt="素材一覧" />
			<span>素材一覧</span>
		</h2> 
		<p class="ln18em">化粧品を入れるパッケージには通常の「紙」以外にも特殊な蒸着紙・ホイル紙や透明のクリアケースなどが用いられます。それぞれの素材によって印刷適性が異なるうえ、印刷する方法によっても印刷後のイメージが異なる為、印刷・素材の両方に経験とノウハウが求められます。</p>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="message-group message-col171 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">                
					<a href="<?php bloginfo('url'); ?>/material#01">
						<img src="<?php bloginfo('template_url'); ?>/img/content/material_navi_img1.jpg" alt="素材一覧" />
					</a> 					        
				</div><!-- end message-col -->
			   
				<div class="message-col">                
					<a href="<?php bloginfo('url'); ?>/material#02">
						<img src="<?php bloginfo('template_url'); ?>/img/content/material_navi_img2.jpg" alt="素材一覧" />
					</a> 					        
				</div><!-- end message-col -->
				
				<div class="message-col">                
					<a href="<?php bloginfo('url'); ?>/material#03">
						<img src="<?php bloginfo('template_url'); ?>/img/content/material_navi_img3.jpg" alt="素材一覧" />
					</a> 					        
				</div><!-- end message-col -->
				
				<div class="message-col">                
					<a href="<?php bloginfo('url'); ?>/material#04">
						<img src="<?php bloginfo('template_url'); ?>/img/content/material_navi_img4.jpg" alt="素材一覧" />
					</a> 					        
				</div><!-- end message-col -->
			</div><!-- end message-row -->
		</div><!-- end message-group -->		
	</div><!-- end primary-row -->
	
	<div id="01" class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">白板紙</h3>
		<p class="mb20 ln18em">化粧品を入れるパッケージには通常の「紙」以外にも特殊な蒸着紙・ホイル紙や透明のクリアケースなどが用いられます。それぞれの素材によって印刷適性が異なるうえ、印刷する方法によっても印刷後のイメージが異なる為、印刷・素材の両方に経験とノウハウが求められます。</p>
		<div class="message-group message-col230 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">                
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/material_content_img1.jpg" alt="素材一覧" />
					</div><!-- end image --> 													
					<div class="text ln18em">
						<p>白板紙の種類は、キャスト紙・アイボリー紙・カードA・カードB・コートボールに分かれ、その用途により種類を使い分けることが可能です。口アイボリー紙以上はバージンパルプ100％が多く、カードA以下は再生紙の割合が多くなっています。</p>
					</div>            					        
				</div><!-- end message-col -->
			   
				<div class="message-col">                
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/material_content_img2.jpg" alt="素材一覧" />
					</div><!-- end image --> 													
					<div class="text ln18em">
						<p>キャスト紙は表面の平滑性が高く、仕上がりがきれいです。</p>
					</div>            					        
				</div><!-- end message-col -->
				
				<div class="message-col">                
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/material_content_img3.jpg" alt="素材一覧" />
					</div><!-- end image --> 													
					<div class="text ln18em">
						<p>カードBは表面の凹凸があります。</p>
					</div>            					        
				</div><!-- end message-col -->				
			</div><!-- end message-row -->
		</div><!-- end message-group -->		
	</div><!-- end primary-row -->
		
	<div id="02" class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">グラビア・パール紙</h3>
		<p class="mb20 ln18em">白紙（板紙）にゴールド・シルバー・パールを印刷し、高級感を演出し光沢ある色彩を表現することができます。<br />偏光パールは見る角度により色が違って見えるものもあります。</p>
		<div class="message-group message-col230 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">                
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/material_content_img4.jpg" alt="素材一覧" />
					</div><!-- end image --> 													
					<div class="text ln18em">
						<p>ホワイト・ゴールド・メタリックゴールド・偏光ブルー・偏光グリーンなどインクも豊富で 様々なパールを施すことが可能です。</p>
					</div>            					        
				</div><!-- end message-col -->
			   
				<div class="message-col">                
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/material_content_img5.jpg" alt="素材一覧" />
					</div><!-- end image --> 													
					<div class="text ln18em">
						<p>パール紙に墨ベタの印刷を施しています。</p>
					</div>            					        
				</div><!-- end message-col -->
				
				<div class="message-col">                
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/material_content_img6.jpg" alt="素材一覧" />
					</div><!-- end image --> 													
					<div class="text ln18em">
						<p>パール紙に藍ベタの印刷を施しています。</p>
					</div>            					        
				</div><!-- end message-col -->				
			</div><!-- end message-row -->
		</div><!-- end message-group -->		
	</div><!-- end primary-row -->
	
	<div id="03" class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">蒸着紙・ホイル紙</h3>
		<p class="mb20 ln18em">白板紙にアルミ蒸着PETやアルミホイル、ホログラムフィルム、アルミ蒸着転写加工を施し、 様々な高級パッケージに使用されています。また乾燥しにくいのも特徴である為、印刷が難しい紙でもあります。</p>
		<div class="message-group message-col230 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">                
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/material_content_img7.jpg" alt="素材一覧" />
					</div><!-- end image --> 													
					<div class="text ln18em">
						<p>蒸着PET層の厚みは12μ・25μがあり、12μよりも25μの方が鏡面度が高く高級感が増します。<br />また、ホイルの厚みは7μ・10μがあります。ロパッケージデザインやグラフィックデザインに対応でき、多様な用途で使用されています。</p>
					</div>            					        
				</div><!-- end message-col -->
			   
				<div class="message-col">                
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/material_content_img8.jpg" alt="素材一覧" />
					</div><!-- end image --> 													
					<div class="text ln18em">
						<p>クリアシート同様に乾燥しづらい紙ですが、弊社はUV印刷なのでブロッキングや密着不良などはありません。</p>
					</div>            					        
				</div><!-- end message-col -->
				
				<div class="message-col">                
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/material_content_img9.jpg" alt="素材一覧" />
					</div><!-- end image --> 													
					<div class="text ln18em">
						<p>蒸着紙・ホイル紙ともツヤ・消しがあります。</p>
					</div>            					        
				</div><!-- end message-col -->				
			</div><!-- end message-row -->
		</div><!-- end message-group -->		
	</div><!-- end primary-row -->
	
	<div id="04" class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">クリアケース</h3>
		<p class="mb20 ln18em">中身が見えるパッケージとして採用されています。PPはPETよりも安価ですが、PETの方が強度があります。<br />PET＝ポリエチレンテレフタレート、PP＝ポリプロピレン　弊社では再生PETも取り扱っています。</p>
		<div class="message-group message-col230 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">                
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/material_content_img10.jpg" alt="素材一覧" />
					</div><!-- end image --> 													
					<div class="text ln18em">
						<p>PETは強度がある為、角が出しやすく製函性が高いのが特徴です。<br />弊社ではオフセット印刷（枚葉）かコンビネーション（フレキソ・輪転）印刷が可能ですので、仕様に合ったご提案ができます。</p>
					</div>            					        
				</div><!-- end message-col -->
			   
				<div class="message-col">                
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/material_content_img11.jpg" alt="素材一覧" />
					</div><!-- end image --> 													
					<div class="text ln18em">
						<p>コンビネーション印刷では表現の可能性が広がります。</p>
					</div>            					        
				</div><!-- end message-col -->
				
				<div class="message-col">                
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/material_content_img12.jpg" alt="素材一覧" />
					</div><!-- end image --> 													
					<div class="text ln18em">
						<p>オフセット印刷は枚葉での印刷になります。</p>
					</div>            					        
				</div><!-- end message-col -->				
			</div><!-- end message-row -->
		</div><!-- end message-group -->		
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','flow_contact');?>
	
<?php get_footer(); ?>
<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="subpages-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/price_top_img.jpg" alt="参考価格" />
			<span>参考価格</span>
		</h2> 
		<p class="ln18em">参考価格例として一例を載せています。<br />
		商品の形状や仕様によってお見積りは変わってきますのであくまでも目安としての価格帯となります。<br />
		お見積りはその都度ご相談ください。</p>		
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','price01') ;?>	
	<?php get_template_part('part','price02') ;?>	
	<?php get_template_part('part','price03') ;?>	
	<?php get_template_part('part','price04') ;?>	
	<?php get_template_part('part','price05') ;?>	
	<?php get_template_part('part','price06') ;?>	
	<?php get_template_part('part','price07') ;?>	
	<?php get_template_part('part','price08') ;?>	
	
	<?php get_template_part('part','flow_contact') ;?>
<?php get_footer(); ?>
		<div id="sack-content-tab">
			<h3 class="h3-title">1.素材を選ぶ</h3>
			<p class="mb20 ln2em">化粧品を入れるパッケージには通常の「紙」以外にも特殊な蒸着紙・ホイル紙や透明のクリアケースなどが用いられます。それぞれの素材によって印刷適性が異なるうえ、印刷する方法によっても印刷後のイメージが異なる為、印刷・素材の両方に経験とノウハウが求められます。</p>
			<div class="message-group message-col171 clearfix"><!-- begin message-group -->
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col">                
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content1_img1.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/material#01">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content1_btn1.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->	

					<div class="message-col">                
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content1_img2.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/material#02">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content1_btn2.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->
					
					<div class="message-col">                
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content1_img3.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/material#03">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content1_btn3.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->
					
					<div class="message-col">                
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content1_img4.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/material#04">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content1_btn4.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->
				</div><!-- end message-row -->
			</div><!-- end message-group -->
		</div><!-- #sack-content-tab -->

		<div id="sack-content-tab">
			<h3 class="h3-title">2.印刷方法を選ぶ</h3>
			<p class="mb20 ln2em">化粧品パッケージはパッケージを見た人が思わず手に取りたくなるようなデザインとそれを表現する印刷技術が求められます。化粧品パッケージ製作.comでは、オフセット印刷・フレキソ印刷・スクリーン印刷の３つの印刷方法を組み合わせることで、デザイナーの皆様が求める品質のパッケージをご提供します。</p>
			<div class="message-group message-col171 clearfix"><!-- begin message-group -->
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col">                
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content2_img1.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/factory#offset">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content2_btn1.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->	

					<div class="message-col">               
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content2_img2.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/factory#flexographic">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content2_btn2.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->
					
					<div class="message-col">                
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content2_img3.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/factory#screen">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content2_btn3.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->
				</div><!-- end message-row -->
			</div><!-- end message-group -->
		</div><!-- #sack-content-tab -->
		
		<div id="sack-content-tab">
			<h3 class="h3-title">3.箔加工</h3>
			<p class="mb20 ln2em">印刷されたパッケージの上に箔を押し重ねることでシルバーやゴールドだけでなく、ホログラム加工も可能です。<br />また、箔押し技術によりパッケージに立体的な凹凸をつけることもできます。</p>
			<div class="message-group message-col171 clearfix"><!-- begin message-group -->
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col">                
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content3_img1.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/display#01">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content3_btn1.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->	

					<div class="message-col">                
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content3_img2.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/display#02">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content3_btn2.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->
					
					<div class="message-col">                
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content3_img3.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/display#03">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content3_btn3.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->
				</div><!-- end message-row -->
			</div><!-- end message-group -->
		</div><!-- #sack-content-tab -->
		
		<div id="sack-content-tab">
			<h3 class="h3-title">4.表面加工</h3>
			<p class="mb20 ln2em">印刷されたパッケージの表面に樹脂やフィルムを張り付けることによって表面に強度を持たせると同時に光沢や様々な模様を付加することが可能になります。</p>
			<div class="message-group message-col171 clearfix"><!-- begin message-group -->
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col">                
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content4_img1.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/display#04">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content4_btn1.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->	

					<div class="message-col">                
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content4_img2.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/display#05">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content4_btn2.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->
					
					<div class="message-col">                
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content4_img3.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/display#06">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content4_btn3.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->
					
					<div class="message-col">                
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content4_img4.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/display#07">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content4_btn4.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->									
				</div><!-- end message-row -->
				
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col">                
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content4_img5.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/display#08">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content4_btn5.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->	

					<div class="message-col">                
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content4_img6.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/display#09">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content4_btn6.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->
					
					<div class="message-col">                
						<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content4_img7.jpg" alt="サック箱" class="pb5"/>
						<a href="<?php bloginfo('url'); ?>/display#10">
							<img src="<?php bloginfo('template_url'); ?>/img/content/sack_content4_btn7.jpg" alt="サック箱" />
						</a> 					        
					</div><!-- end message-col -->																		
				</div><!-- end message-row -->
			</div><!-- end message-group -->
		</div><!-- #sack-content-tab -->	
	
	
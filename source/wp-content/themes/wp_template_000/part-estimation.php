	<div class="primary-row clearfix">
		<h3 class="h3-title">見積りを依頼する</h3>
		<?php echo do_shortcode('[contact-form-7 id="47" title="お見積り"]') ?>						
	</div><!-- end primary-row -->

	
	<div style="display:none;">
		<select id="thinkness_courtball">		
			<optgroup label="紙厚を選ぶ">
				<option value="コートボール23.5kg">コートボール23.5kg</option>
				<option value="コートボール27kg">コートボール27kg</option>
				<option value="コートボール31kg">コートボール31kg</option>		
			</optgroup>		
		</select>
		
		<select id="thickness_ivory">		
			<optgroup label="紙厚を選ぶ">
				<option value="アイボリー23.5kg">アイボリー23.5kg</option>
				<option value="アイボリー27kg">アイボリー27kg</option>
				<option value="アイボリー31kg">アイボリー31kg</option>		
			</optgroup>		
		</select>
		
		<select id="thickness_carda">		
			<optgroup label="紙厚を選ぶ">
				<option value="カードA23.5kg">カードA23.5kg</option>
				<option value="カードA27kg">カードA27kg</option>
				<option value="カードA31kg">カードA31kg</option>		
			</optgroup>		
		</select>
		
		<select id="thickness_cardb">		
			<optgroup label="紙厚を選ぶ">
				<option value="カードB23.5kg">カードB23.5kg</option>
				<option value="カードB27kg">カードB27kg</option>
				<option value="カードB31kg">カードB31kg</option>		
			</optgroup>		
		</select>
		
		<select id="thickness_cardpaper">		
			<optgroup label="紙厚を選ぶ">
				<option value="キャスト紙23.5kg">キャスト紙23.5kg</option>
				<option value="キャスト紙27kg">キャスト紙27kg</option>
				<option value="キャスト紙31kg">キャスト紙31kg</option>		
			</optgroup>		
		</select>
		
		<select id="thickness_depositedpaper">		
			<optgroup label="紙厚を選ぶ">
				<option value="蒸着紙23.5kg">蒸着紙23.5kg</option>
				<option value="蒸着紙27kg">蒸着紙27kg</option>
				<option value="蒸着紙31kg">蒸着紙31kg</option>		
			</optgroup>		
		</select>
		
		<select id="thickness_clearcase">		
			<optgroup label="紙厚を選ぶ">
				<option value="クリアケース23.5kg">クリアケース23.5kg</option>
				<option value="クリアケース27kg">クリアケース27kg</option>
				<option value="クリアケース31kg">クリアケース31kg</option>		
			</optgroup>		
		</select>
	</div>
	
	<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>	
	<script type = 'text/javascript'> 
		$( document ).ready(function() {
			$('#zip').change(function(){					
				//AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
	    		AjaxZip3.zip2addr(this,'','pref','addr1','addr2');
	  		});
				
			$('input[name="radio_shape"][value="窓貼り"]').click(function(){
				$('#material-info1').show();
				$('#material-info2').hide();				
			});
			
			$('input[name="radio_shape"][value="サック箱"],[value="ワンタッチ底"],[value="地獄底"],[value=" ピローケース"],[value=" 看板付き"],[value="台紙"],[value="特殊形状"],[value="その他"]').click(function(){
				$('#material-info1').show();
				$('#material-info2').show();
			});
								
			$('select[name="thinkness_courtball"]').html($("#thinkness_courtball").html()).show();			
			$('select[name="thickness_ivory"]').html($("#thickness_ivory").html()).hide();
			$('select[name="thickness_carda"]').html($("#thickness_carda").html()).hide();
			$('select[name="thickness_cardb"]').html($("#thickness_cardb").html()).hide();
			$('select[name="thickness_cardpaper"]').html($("#thickness_cardpaper").html()).hide();
			$('select[name="thickness_depositedpaper"]').html($("#thickness_depositedpaper").html()).hide();
			$('select[name="thickness_clearcase"]').html($("#thickness_clearcase").html()).hide();	
			$('input:radio[name="radio_material"]').click(function(){
				var print_index = $('input:radio[name="radio_material"]').index(this);
				if(print_index == 0){						
					$('select[name="thinkness_courtball"]').html($("#thinkness_courtball").html()).show();															
					$('select[name="thickness_ivory"]').html($("#thickness_ivory").html()).hide();
					$('select[name="thickness_carda"]').html($("#thickness_carda").html()).hide();
					$('select[name="thickness_cardb"]').html($("#thickness_cardb").html()).hide();
					$('select[name="thickness_cardpaper"]').html($("#thickness_cardpaper").html()).hide();
					$('select[name="thickness_depositedpaper"]').html($("#thickness_depositedpaper").html()).hide();
					$('select[name="thickness_clearcase"]').html($("#thickness_clearcase").html()).hide();		
					$('.thinkness_courtball').show();
				}
				else if(print_index==1){
					$('select[name="thinkness_courtball"]').html($(".thinkness_courtball").html()).hide();
					$('select[name="thickness_ivory"]').html($("#thickness_ivory").html()).show();
					$('select[name="thickness_carda"]').html($("#thickness_carda").html()).hide();
					$('select[name="thickness_cardb"]').html($("#thickness_cardb").html()).hide();
					$('select[name="thickness_cardpaper"]').html($("#thickness_cardpaper").html()).hide();
					$('select[name="thickness_depositedpaper"]').html($("#thickness_depositedpaper").html()).hide();
					$('select[name="thickness_clearcase"]').html($("#thickness_clearcase").html()).hide();			
					$('.thickness_ivory').show();
				}
				else if(print_index==2){
					$('select[name="thinkness_courtball"]').html($("#thinkness_courtball").html()).hide();
					$('select[name="thickness_ivory"]').html($("#thickness_ivory").html()).hide();
					$('select[name="thickness_carda"]').html($("#thickness_carda").html()).show();
					$('select[name="thickness_cardb"]').html($("#thickness_cardb").html()).hide();
					$('select[name="thickness_cardpaper"]').html($("#thickness_cardpaper").html()).hide();
					$('select[name="thickness_depositedpaper"]').html($("#thickness_depositedpaper").html()).hide();
					$('select[name="thickness_clearcase"]').html($("#thickness_clearcase").html()).hide();			
					$('.thickness_carda').show();
				}
				else if(print_index==3){
					$('select[name="thinkness_courtball"]').html($("#thinkness_courtball").html()).hide();
					$('select[name="thickness_ivory"]').html($("#thickness_ivory").html()).hide();
					$('select[name="thickness_carda"]').html($("#thickness_carda").html()).hide();
					$('select[name="thickness_cardb"]').html($("#thickness_cardb").html()).show();
					$('select[name="thickness_cardpaper"]').html($("#thickness_cardpaper").html()).hide();
					$('select[name="thickness_depositedpaper"]').html($("#thickness_depositedpaper").html()).hide();
					$('select[name="thickness_clearcase"]').html($("#thickness_clearcase").html()).hide();			
					$('.thickness_cardb').show();
				}
				else if(print_index==4){
					$('select[name="thinkness_courtball"]').html($("#thinkness_courtball").html()).hide();
					$('select[name="thickness_ivory"]').html($("#thickness_ivory").html()).hide();
					$('select[name="thickness_carda"]').html($("#thickness_carda").html()).hide();
					$('select[name="thickness_cardb"]').html($("#thickness_cardb").html()).hide();
					$('select[name="thickness_cardpaper"]').html($("#thickness_cardpaper").html()).show();
					$('select[name="thickness_depositedpaper"]').html($("#thickness_depositedpaper").html()).hide();
					$('select[name="thickness_clearcase"]').html($("#thickness_clearcase").html()).hide();			
					$('.thickness_cardpaper').show();
				}
				else if(print_index==5){
					$('select[name="thinkness_courtball"]').html($("#thinkness_courtball").html()).hide();
					$('select[name="thickness_ivory"]').html($("#thickness_ivory").html()).hide();
					$('select[name="thickness_carda"]').html($("#thickness_carda").html()).hide();
					$('select[name="thickness_cardb"]').html($("#thickness_cardb").html()).hide();
					$('select[name="thickness_cardpaper"]').html($("#thickness_cardpaper").html()).hide();
					$('select[name="thickness_depositedpaper"]').html($("#thickness_depositedpaper").html()).show();
					$('select[name="thickness_clearcase"]').html($("#thickness_clearcase").html()).hide();			
					$('.thickness_depositedpaper').show();
				}
				else{
					$('select[name="thinkness_courtball"]').html($("#thinkness_courtball").html()).hide();
					$('select[name="thickness_ivory"]').html($("#thickness_ivory").html()).hide();
					$('select[name="thickness_carda"]').html($("#thickness_carda").html()).hide();
					$('select[name="thickness_cardb"]').html($("#thickness_cardb").html()).hide();
					$('select[name="thickness_cardpaper"]').html($("#thickness_cardpaper").html()).hide();
					$('select[name="thickness_depositedpaper"]').html($("#thickness_depositedpaper").html()).hide();
					$('select[name="thickness_clearcase"]').html($("#thickness_clearcase").html()).show();			
					$('.thickness_clearcase').show();
				}
			});				
		})
	</script>	
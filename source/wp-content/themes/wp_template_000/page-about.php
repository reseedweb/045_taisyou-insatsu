<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="subpages-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/about_top_img.jpg" alt="初めての方へ" />
			<span>初めての方へ</span>
		</h2> 
		<p class="ln18em">当社の強みはお客様ひとりひとりにあわせた提案力です！<br />
			化粧品パッケージ製作.comでは、「高品質」「スピード対応で短納期にも対応」「制作実績月産500万個以上の確かな実績」この3つを柱に、幅広い提案力でご要望、サイズ・形状・デザイン・素材にいたるまでお任せいただけるので、内容物にあわせてぴったりの印刷、パッケージ製作ができます！</p>
		<div class="about-content-top clearfix">						
			<h3 class="about-contop-ltitle">お悩み</h3>
			<h3 class="about-contop-rtitle">解決策</h3>
			<ul class="about-contop-ltext">
				<li>サイズ・加工がわからない</li>
				<li>思った通りに<br />化粧品パッケージを<br />作りたい</li>
				<li>コストを抑えたい</li>
				<li>納期に間に合わせたい</li>
			</ul>
			<ul class="about-contop-rtext">
				<li>内容物や使用するシチュエーションなどをヒアリングし、<span class="about-rtext-clr">最適なサイズ・形状をご提案</span>いたします。</li>
				<li>どのような化粧品パッケージをご希望でしょうか？イメージや参考の商品・ブランドをお教えください。<span class="about-rtext-clr">完全オリジナルの化粧品パッケージ製作が可能</span>です。</li>
				<li>コスト削減のご相談もお任せください。紙の素材や形状などをお任せいただければ、<span class="about-rtext-clr">最安値でのお見積りも可能</span>です。お問合せの際にご予算などもお伝えください。</li>
				<li>ご希望の納期に沿って製作いたします。特にお急ぎの方もまずはご連絡ください。<span class="about-rtext-clr">どんな短納期での製作も可能</span>な場合がございます。まずはご相談ください。</li>
			</ul>
		</div>
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<h3 class="about-title">1.自社一貫生産で全数検査及び混入対策で品質管理を徹底し高品質！</h3> 
		<div class="message-left message-308 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_img1.jpg" alt="初めての方へ" />
			</div>
			<div class="text ln18em">
				<p>化粧品パッケージ製作.comではパッケージ製作のほぼ全ての工程を自社工場内で行い全数検査及び混入対策を講じた品質管理を徹底し、品質を気にされるお客様にも満足いただいております。</p>
			</div>
		</div><!-- end message-308 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">2.スピード対応でお急ぎの短納期の製作もOK!</h3> 
		<div class="message-left message-308 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_img2.jpg" alt="初めての方へ" />
			</div>
			<div class="text ln18em">
				<p>化粧品パッケージ製作.comでは納期をお急ぎな案件に関しても、可能な限り短納期で対応いたします。1000個までの小ロットならデータ校了後最短５営業日での発送が可能です。お気軽にご相談ください。</p>
			</div>
		</div><!-- end message-308 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">3.制作実績月産500万個以上!月間1000件の確かな実績と経験</h3> 
		<div class="message-left message-308 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_img3.jpg" alt="初めての方へ" />
			</div>
			<div class="text ln18em">
				<p>「作ってみたいパッケージがある」「予算感や仕様などの詳細が知りたい」など少しでも興味をお持ちの方は、企画段階でのご相談でも構いませんので、お問い合わせフォームやお電話からお気軽にご相談ください。<br />
				幅広い提案力でお客様に満足頂いております。</p>
			</div>
		</div><!-- end message-308 -->
	</div><!-- end primary-row -->
	
	
	<?php get_template_part('part','flow_contact');?>
	
<?php get_footer(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">台紙</h3>
		<table class="price-table">
			<tr>
				<th>ロット数</th>
				<th>１色単価例</th>
				<th>4色単価例</th>
				<th>6色単価例</th>
			</tr>
			<tr>
				<th>1,000</th>
				<td>50円</td>
				<td>120円</td>				
				<td>120円</td>
			</tr>
			<tr>
				<th>5,000</th>
				<td>15円</td>
				<td>28円</td>				
				<td>28円</td>
			</tr>
			<tr>
				<th>10,000</th>
				<td>7円</td>
				<td>14.5円</td>				
				<td>14.5円</td>
			</tr>
		</table><!-- .price-table -->
	</div><!-- end primary-row -->
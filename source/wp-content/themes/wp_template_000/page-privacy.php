<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="subpages-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/privacy_top_img.jpg" alt="プライバシーポリシー" />
			<span>プライバシーポリシー</span>
		</h2> 
		<p class="ln18em">化粧品パッケージ製作.comは、印刷業としての使命と責任を十分に自覚し、個人情報の保護に対して厳重かつ適切な管理体制を敷く責任を負うものと認識して、以下の通り個人情報保護方針を制定し、これを実行、維持、改善いたします。</p>		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">１．法令及びその他の規範の遵守</h3>
		<div class="privacy-content clearfix">
			<p>ご利用になるお客様のプライバシー及び個人情報の保護にあたり、適用 される法令及びその他の規範を遵守いたします。</p>
		</div>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">２．個人情報の取得・利用</h3>
		<div class="privacy-content clearfix">
			<p>ご利用になるお客様から個人情報をご提供いただくときは、提供サービス ごとにその利用目的をあらかじめ明示し適正な方法で取得します。<br />
			ご提供いただいた個人情報は、明示した利用目的の範囲で利用します。</p>
		</div>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">３．個人情報の提供</h3>
		<div class="privacy-content clearfix">
			<p>ご利用になるお客様からご提供いただきました個人情報を第三者に開示又は 提供いたしません。<br />ただし、法令に基づく場合など、正当な理由がある場合を 除きます。</p>
		</div>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">４．個人参加・公開</h3>
		<div class="privacy-content clearfix">
			<p>保有する個人情報を正確かつ最新の内容に保ちます。 また、ご利用になるお客様本人から私が保有する個人情報の開示、訂正又は削除 を求められたときは、ご本人確認後、速やかにこれに応じます。<br />
			ただし、法令又は規約等により、個人情報の保存期間が定められているときは、 保存期間の経過後に削除します。</p>
		</div>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">５．安全管理対策</h3>
		<div class="privacy-content clearfix">
			<p>収集した個人情報に対し、不正アクセス、紛失、破壊、改ざん及び 漏えい等の予防並びに是正措置を講じ、厳正な管理の下で安全に蓄積・保管します。</p>
		</div>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">６．継続的改善</h3>
		<div class="privacy-content clearfix">
			<p>個人情報保護に関するコンプライアンス・プログラムを継続的に見直し改善を行います。</p>
		</div>
	</div><!-- end primary-row -->	
	
	<?php get_template_part('part','flow_contact');?>
	
<?php get_footer(); ?>
<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="subpages-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/kanban_top_img.jpg" alt="看板付き" />
			<span>看板付き</span>
		</h2> 
		
		<div class="sack-top-left clearfix">
			<div id="mainalbum">										
				<span><img src="<?php bloginfo('template_url'); ?>/img/content/kanban_slide_img1.jpg" alt="看板付き" class="mainImage" /></span>
			</div>
			<div id="subalbum">
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/kanban_slide_img1.jpg" alt="看板付き" class="thumb" name="mainImage" /></p>
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/kanban_slide_img2.jpg" alt="看板付き" class="thumb" name="mainImage" /></p>
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/kanban_slide_img3.jpg" alt="看板付き" class="thumb" name="mainImage" /></p>
			</div>
		</div><!-- .sack-top-left -->		
		
		<div class="sack-top-right ln18em clearfix">
			<p>店舗販売などの陳列スペース用として使われています。ヘッダー付きとも言われ、アイキャッチとして使用されることが多いタイプです。</p>
		</div>	
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="sack-tabs htabs clearfix">
			<a href="#guide" class="guide">仕様・制作ガイド<i class="fa fa-arrow-down"></i></a>
			<a href="#price" class="price">参考価格例<i class="fa fa-arrow-down"></i></a>
			<a href="#estimate" class="estimate">お見積りフォーム<i class="fa fa-arrow-down"></i></a>										
		</div>	
	</div><!-- end primary-row -->
	
	<div id="guide" class="primary-row clearfix">
		<?php get_template_part('part','guide1') ;?>	
	</div><!-- #guide -->
	
	<div id="price" class="primary-row clearfix">
		<?php get_template_part('part','price05') ;?>	
	</div><!-- #price -->
	
	<div id="estimate" class="primary-row clearfix">		
		<?php get_template_part('part','estimation'); ?>	
	</div><!-- #estimate -->
	
	<?php get_template_part('part','flow_contact');?>
	
<?php get_footer(); ?>
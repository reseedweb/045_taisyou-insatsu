<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">お問合せ、お見積りのご依頼はコチラから</h2> 
	<h3 class="topflow-top-title">お問合せから納品までの流れ</h3>
	<ul class="topflow-list1 clearfix">
		<li>
			<a href="<?php bloginfo('url'); ?>/flow#01">
				<h4 class="topflow-step">step1</h4>
				<div class="topflow-info clearfix">
					<p class="topflow-img"><img src="<?php bloginfo('template_url'); ?>/img/top/topflow_img1.jpg" alt="<?php bloginfo('name');?>" /></p>
					<h5 class="topflow-title">お問合せ</h5>
					<p class="topflow-text">まずはお気軽にお問い合わせください。価格のご相談や、パッケージ以外のご依頼もお待ちしております。</p>
				</div>					
			</a>				
		</li>	
		<div class="topflow-farrow"><img src="<?php bloginfo('template_url'); ?>/img/top/topflow_farrow.jpg" alt="<?php bloginfo('name');?>" align="middle"/></div>		
		<li>
			<a href="<?php bloginfo('url'); ?>/flow#02">
				<h4 class="topflow-step">step2</h4>
				<div class="topflow-info clearfix">
					<p class="topflow-img"><img src="<?php bloginfo('template_url'); ?>/img/top/topflow_img2.jpg" alt="<?php bloginfo('name');?>" /></p>
					<h5 class="topflow-title">お見積り</h5>
					<p class="topflow-text">打ち合わせ内容でお聞きしたご要望より、オリジナルパッケージのお見積りをお伝えさせていただきます。</p>
				</div>					
			</a>				
		</li>
		<div class="topflow-sarrow"><img src="<?php bloginfo('template_url'); ?>/img/top/topflow_farrow.jpg" alt="<?php bloginfo('name');?>" align="middle"/></div>		
		<li>
			<a href="<?php bloginfo('url'); ?>/flow#03">
				<h4 class="topflow-step">step3</h4>
				<div class="topflow-info clearfix">
					<p class="topflow-img"><img src="<?php bloginfo('template_url'); ?>/img/top/topflow_img3.jpg" alt="<?php bloginfo('name');?>" /></p>
					<h5 class="topflow-title">サンプル製作</h5>
					<p class="topflow-text">パッケージのサンプル品を作成し実際に内容物を入れていただき、サイズ・形状・仕様などに問題ないかご確認いたます。</p>
				</div>					
			</a>				
		</li>		
	</ul>
	<div class="topflow-line"><img src="<?php bloginfo('template_url'); ?>/img/top/topflow_line.jpg" alt="<?php bloginfo('name');?>" align="middle"/></div>		
	<ul class="topflow-list2 clearfix">
		<li>
			<a href="<?php bloginfo('url'); ?>/flow#04">
				<h4 class="topflow-step">step4</h4>
				<div class="topflow-info clearfix">
					<p class="topflow-img"><img src="<?php bloginfo('template_url'); ?>/img/top/topflow_img4.jpg" alt="<?php bloginfo('name');?>" /></p>
					<h5 class="topflow-title">ご注文</h5>
					<p class="topflow-text">決定した仕様に基づいての見積りをご提出します。見積りに納得いただきましたら、正式にご注文となります。</p>
				</div>					
			</a>				
		</li>	
		<div class="topflow-tarrow"><img src="<?php bloginfo('template_url'); ?>/img/top/topflow_farrow.jpg" alt="<?php bloginfo('name');?>" align="middle"/></div>		
		<li>
			<a href="<?php bloginfo('url'); ?>/flow#05">
				<h4 class="topflow-step">step5</h4>
				<div class="topflow-info clearfix">
					<p class="topflow-img"><img src="<?php bloginfo('template_url'); ?>/img/top/topflow_img5.jpg" alt="<?php bloginfo('name');?>" /></p>
					<h5 class="topflow-title">データ入稿・箱製造</h5>
					<p class="topflow-text">パッケージのデザインデータをご入稿ください。自社工場によりパッケージの製造を行います。</p>
				</div>					
			</a>				
		</li>
		<div class="topflow-earrow"><img src="<?php bloginfo('template_url'); ?>/img/top/topflow_earrow.jpg" alt="<?php bloginfo('name');?>" align="middle"/></div>		
		<li>
			<a href="<?php bloginfo('url'); ?>/flow#06">
				<h4 class="topflow-step">step6</h4>
				<div class="topflow-info clearfix">
					<p class="topflow-img"><img src="<?php bloginfo('template_url'); ?>/img/top/topflow_img6.jpg" alt="<?php bloginfo('name');?>" /></p>
					<h5 class="topflow-title">納品</h5>
					<p class="topflow-text">製造された箱は丁寧に検品が行われ出荷されます。全国のお客様へ発送、指定日にあわせて納品いたします。</p>
				</div>					
			</a>				
		</li>				
	</ul>
	<div class="mt20">
        <a href="<?php bloginfo('url'); ?>/contact"><img src="<?php bloginfo('template_url'); ?>/img/top/top_contact.jpg" alt="<?php bloginfo('name');?>" /></a>
    </div>
</div><!-- end primary-row -->   
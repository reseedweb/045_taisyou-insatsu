<div class="sidebar-row clearfix">
	<h2 class="side-title">ご相談&お問合せ</h2>
	<a href="<?php bloginfo('url'); ?>/contact">
		<img src="<?php bloginfo('template_url'); ?>/img/common/side_contact.jpg" alt="<?php bloginfo('name');?>" />
	</a>
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->    
    <?php get_template_part('part','sideMenu'); ?>
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix">	
	<a href="<?php bloginfo('url'); ?>/price">
		<img src="<?php bloginfo('template_url'); ?>/img/common/side_price.jpg" alt="<?php bloginfo('name');?>" />
	</a>
	<p class="mt20">
		<a href="<?php bloginfo('url'); ?>/work">
			<img src="<?php bloginfo('template_url'); ?>/img/common/side_work.jpg" alt="<?php bloginfo('name');?>" />
		</a>
	</p>
	<p class="mt20">
		<a href="<?php bloginfo('url'); ?>/blog">
			<img src="<?php bloginfo('template_url'); ?>/img/common/side_blog.jpg" alt="<?php bloginfo('name');?>" />
		</a>
	</p>	
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->    
    <?php get_template_part('part','sideNav'); ?>
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix">	
	<a href="http://cosmepackage.com/" target="_blank">
		<img src="<?php bloginfo('template_url'); ?>/img/common/side_link.jpg" alt="<?php bloginfo('name');?>" />
	</a>
</div><!-- end sidebar-row -->
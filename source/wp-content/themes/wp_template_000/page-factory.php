<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="subpages-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/factory_top_img.jpg" alt="工場紹介" />
			<span>工場紹介</span>
		</h2> 
				
		<p class="ln18em">
			弊社ではパッケージの制作に特化した工場設備を自社で保有しております。<br />
			様々な加工に対応できる機械を各複数用意していますので、多様な要望に短納期でお応えすることができます。<br />
			化粧品パッケージはパッケージを見た人が思わず手に取りたくなるようなデザインとそれを表現する印刷技術が求められます。オフセット印刷・フレキソ印刷・スクリーン印刷の３つの印刷方法を組み合わせることで、デザイナーの皆様が求める品質のパッケージをご提供します。
		</p>
	</div><!-- end primary-row -->
	

	
	<div id="tabs" class="primary-row clearfix"><!-- begin primary-row -->
		<ul class="factory-tabs clearfix">
			<li><a href="#offset" class="offset">オフセット印刷<i class="fa fa-arrow-down"></i></a></li>
			<li><a href="#flexographic" class="flexographic">フレキソ印刷<i class="fa fa-arrow-down"></i></a><li>
			<li><a href="#screen" class="screen">スクリーン印刷<i class="fa fa-arrow-down"></i></a>										</li>
		</ul>		
	
		<div id="offset" class="mt40">
			<h3 class="h3-title">オフセット印刷</h3>
			<p class="mb30">UVオフセット印刷30年以上のノウハウ。再現性に優れ、高精細な印刷も可能！</p>
			<div class="videoWrapper">
				<iframe src="https://www.youtube.com/embed/b9VBn5VmdHo?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>		
			
			<div id="factory-content">
				<div class="factory-point1 first-child clearfix">
					<div class="fac-point1-left">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_point1_img1.jpg" alt="工場紹介" />
						<span>再現性</span>
					</div>
					<div class="fac-point1-right">大量印刷に向き、カラー印刷の表現にも優れています。FMスクリーンによる高精細な印刷も可能です。</div>
				</div><!-- .factory-point1 -->
				<div class="factory-point1 clearfix">
					<div class="fac-point1-left">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_point1_img2.jpg" alt="工場紹介" />
						<span>美粧性</span>
					</div>
					<div class="fac-point1-right">ホットスタンプによる箔押しで、ロスの少ない生産が可能です。</div>
				</div><!-- .factory-point1 -->
				<div class="factory-point1 clearfix">
					<div class="fac-point1-left">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_point1_img3.jpg" alt="工場紹介" />
						<span>コスト</span>
					</div>
					<div class="fac-point1-right">展開寸法が大きい箱も印刷可能であり、大量生産によるコスト削減が可能です。</div>
				</div><!-- .factory-point1 -->
				<div class="factory-point1 clearfix">
					<div class="fac-point1-left">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_point1_img4.jpg" alt="工場紹介" />
						<span>エコ</span>
					</div>
					<div class="fac-point1-right">大ロットの印刷の場合、材料ロスが少なめで押さえることが可能です。</div>
				</div><!-- .factory-point1 -->
			</div><!-- #factory-content -->
			
			<div id="factory-content">
				<ul class="facoffset-point2 clearfix">				
					<li>現在の印刷で最も一般的な印刷方式です。</li>
					<li>枚葉とは、規格の寸法に切りそろえられた紙のことです。</li>
					<li>版は、一般的には薄いアルミのPS版（平版）を使用します。</li>
					<li>オフセットは、版と紙が直接触れないのが特徴で、</li>
					<li>ゴムブランケットに転写（OFF）してから紙に転写（SET）する印刷方式です。</li>
				</ul>				
			</div><!-- #factory-content -->

			<div id="factory-content">
				<div class="facoffset-img clearfix">
					<div class="facoffset-img-left"><img src="<?php bloginfo('template_url'); ?>/img/content/facoffset_content_img1.jpg" alt="工場紹介" /></div>
					<div class="facoffset-img-right"><img src="<?php bloginfo('template_url'); ?>/img/content/facoffset_content_img2.jpg" alt="工場紹介" /></div>
				</div>
			</div><!-- #factory-content -->
			
			<div id="factory-content">
				<h4 class="factory-subtitle">組み合わせが多様な枚葉印刷</h4>
				<p>
					弊社では、グラビア印刷（グラビアパール）、ホットスタンプ、エンボス、シルクスクリーン印刷など、枚葉印刷が可能な二次加工を組み合わせることができます。<br />
					※一部加工は外部の協力会社に依頼しています。
				</p>
			</div><!-- #factory-content -->
			
			<div id="factory-content" class="mb40">
				<h4 class="factory-subtitle">オフセット印刷の種類</h4>
				<p>
					オフセットインキには油性タイプとUVタイプの2種類があります。<br />
					弊社では品質、作業効率、後加工の安定性を考えて、UVオフセット印刷を行っています。
				</p>
				<table class="factory-table">
					<tr>
						<th></th>
						<th>印刷適正</th>
						<th>UV</th>
					</tr>
					<tr>
						<th class="factory-th-clr">印刷適正</th>
						<td>水幅が大きく印刷がしやすい</td>
						<td>水幅が小さく安定し難い</td>
					</tr>
					<tr>
						<th>乾燥システム</th>
						<td>
							自然乾燥<br />
							 （裏移り防止に、パウダーを散布）
						</td>
						<td>
							UV装置による紫外線でラジカル反応硬化
						</td>
					</tr>
					<tr>
						<th class="factory-th-clr">
							後加工適正<br />
							 （表面加工）
						</th>
						<td>
							パウダーで白点の模様が発生することがあり、化粧品等の高級パッケージには使い難い
						</td>
						<td>ラジカル反応で瞬時乾燥させるために、水とインキ量の配分を間違えると、気泡が入る</td>
					</tr>
					<tr>
						<th>費用</th>
						<td>インキ代はUVインキの1/5で非常に安い</td>
						<td>UV装置が非常に高価である</td>
					</tr>
					<tr>
						<th class="factory-th-clr">用紙</th>
						<td>水を吸い込む用紙に使用、ホイル紙等に印刷すると、乾燥に数日要する</td>
						<td>
							用紙を選ばない<br />
							ホイル紙・パール紙・PET・PP
						</td>
					</tr>
				</table>
			</div><!-- #factory-content -->		
		</div><!-- #offset -->
		
		
		<div id="flexographic" class="mt40">
			<h3 class="h3-title">フレキソ印刷</h3>
			<p class="mb30">エコロジー・スピーディ・ハイ・クオリティでフレキソ印刷＋αへの挑戦！</p>		
			
			<div class="videoWrapper">
				<iframe src="https://www.youtube.com/embed/9vjFdPf1Gmw?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>
			
			<div id="factory-content">
				<div class="factory-point1 first-child clearfix">
					<div class="fac-point1-left">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_point1_img1.jpg" alt="工場紹介" />
						<span>再現性</span>
					</div>
					<div class="fac-point1-right">水を使わない為、色ムラが発生しにくく、極小文字まで再現が可能で、特に白・墨ベタの印刷はキレイに再現されます。</div>
				</div><!-- .factory-point1 -->
				<div class="factory-point1 clearfix">
					<div class="fac-point1-left">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_point1_img2.jpg" alt="工場紹介" />
						<span>美粧性</span>
					</div>
					<div class="fac-point1-right">コールドフォイルを使えば、箔の上からさらに印刷をすることができ、表現の幅を広げることができます。</div>
				</div><!-- .factory-point1 -->
				<div class="factory-point1 clearfix">
					<div class="fac-point1-left">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_point1_img3.jpg" alt="工場紹介" />
						<span>コスト</span>
					</div>
					<div class="fac-point1-right">PET・PPシートはオフセット印刷に比べて原材料費を安く調達することができます。</div>
				</div><!-- .factory-point1 -->
				<div class="factory-point1 clearfix">
					<div class="fac-point1-left">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_point1_img4.jpg" alt="工場紹介" />
						<span>エコ</span>
					</div>
					<div class="fac-point1-right">印刷資材にアルコール・有機溶剤を使用しておりませんので、環境負荷を軽減できます。</div>
				</div><!-- .factory-point1 -->
			</div><!-- #factory-content -->
			
			<div id="factory-content">
				<ul class="facflex-point2 clearfix">				
					<li>版は樹脂版で弾力のある版を使用します。</li>
					<li>印刷方式は凸版印刷で一般的な凸印刷に比べ印圧が軽くキスタッチと呼ばれています。</li>
					<li>アニロックスロールという細かいメッシュの彫刻ロールを使うことでインキを均一に転移させます。<br />
						※アニロックスロールはオフセット印刷のインキつぼの役割を果たし、ロールを変更するだけでインキ量を変えられます。同じインキを使用しても違う印刷色の表現が可能です。
					</li>
					<li>インキは溶剤系のインキではなくUVインキを使用し、また水を使わないため、環境にも優しい印刷方法です。</li>
				</ul>				
			</div><!-- #factory-content -->
			
			<div id="factory-content">
				<h4 class="factory-subtitle">フレキソ印刷で表現可能な加工</h4>
				<p class="mb30">
					弊社では、グラビア印刷（グラビアパール）、ホットスタンプ、エンボス、シルクスクリーン印刷など、枚葉印刷が可能な二次加工を組み合わせることができます。<br />
					※一部加工は外部の協力会社に依頼しています。
				</p>
				
				<div class="message-group message-col230 clearfix"><!-- begin message-group -->
					<div class="message-row clearfix"><!--message-row -->
						<div class="message-col">                
							<p><img src="<?php bloginfo('template_url'); ?>/img/content/facflex_content_img1.jpg" alt="工場紹介"/></p>
							<h5 class="factory-ctitle">クリアケース</h5>					        
						</div><!-- end message-col -->	
						<div class="message-col">                
							<p><img src="<?php bloginfo('template_url'); ?>/img/content/facflex_content_img2.jpg" alt="工場紹介"/></p>
							<h5 class="factory-ctitle">パール</h5>
						</div><!-- end message-col -->
						<div class="message-col">                
							<p><img src="<?php bloginfo('template_url'); ?>/img/content/facflex_content_img3.jpg" alt="工場紹介"/></p>
							<h5 class="factory-ctitle">擬似エンボス</h5>
						</div><!-- end message-col -->
					</div><!-- end message-row -->
					
					<div class="message-row clearfix"><!--message-row -->
						<div class="message-col mt20">                
							<p><img src="<?php bloginfo('template_url'); ?>/img/content/facflex_content_img4.jpg" alt="工場紹介"/></p>
							<h5 class="factory-ctitle">LC</h5>
						</div><!-- end message-col -->	
						<div class="message-col mt20">                
							<p><img src="<?php bloginfo('template_url'); ?>/img/content/facflex_content_img5.jpg" alt="工場紹介"/></p>
							<h5 class="factory-ctitle">ラベル</h5>
						</div><!-- end message-col -->	
						<div class="message-col mt20">                
							<p><img src="<?php bloginfo('template_url'); ?>/img/content/facflex_content_img6.jpg" alt="工場紹介"/></p>
							<h5 class="factory-ctitle">軟包材</h5>
						</div><!-- end message-col -->		
					</div><!-- end message-row -->
					
					<div class="message-row clearfix"><!--message-row -->
						<div class="message-col mt20">                
							<p><img src="<?php bloginfo('template_url'); ?>/img/content/facflex_content_img7.jpg" alt="工場紹介"/></p>
							<h5 class="factory-ctitle">クリアファイル</h5>
						</div><!-- end message-col -->	
					</div><!-- end message-row -->
				</div><!-- end message-group -->
			</div><!-- #factory-content -->
			
			<div id="factory-content" class="mb40">
				<h4 class="facflex-subtitle">弊社のフレキソ印刷機の特徴</h4>
				<div class="facflex-last clearfix">
					<h5 class="facflex-ltitle">
						<img src="<?php bloginfo('template_url'); ?>/img/content/facflex_step1.jpg" alt="工場紹介"/>
						<span>粒子の大きなインキ</span>
					</h5>
					<p>シルバー・パール・メタシャイン（ガラス）等の印刷ができます。</p>
				</div><!-- .facflex-last -->
				<div class="facflex-last clearfix">
					<h5 class="facflex-ltitle">
						<img src="<?php bloginfo('template_url'); ?>/img/content/facflex_step2.jpg" alt="工場紹介"/>
						<span>コールドファイル</span>
					</h5>
					<p>印刷用の版を使って、ほぼ印刷と同じ絵柄・文字等が再現できます。また、箔上に着色が可能なため、特注箔を作る必要がありません。</p>
				</div><!-- .facflex-last -->
				<div class="facflex-last clearfix">
					<h5 class="facflex-ltitle">
						<img src="<?php bloginfo('template_url'); ?>/img/content/facflex_step3.jpg" alt="工場紹介"/>
						<span>LCコート</span>
					</h5>
					<p>ホロフィルム等の柄を、製品に転写します。（柄は問い合わせください）</p>
				</div><!-- .facflex-last -->
				<div class="facflex-last clearfix">
					<h5 class="facflex-ltitle">
						<img src="<?php bloginfo('template_url'); ?>/img/content/facflex_step4.jpg" alt="工場紹介"/>
						<span>反転バー</span>
					</h5>
					<p>用紙を反転させ、両面同時印刷が可能です。</p>
				</div><!-- .facflex-last -->
				<div class="facflex-last clearfix">
					<h5 class="facflex-ltitle">
						<img src="<?php bloginfo('template_url'); ?>/img/content/facflex_step5.jpg" alt="工場紹介"/>
						<span>擬似エンボス</span>
					</h5>
					<p>UVコート等のコーティングが出来るので、疑似加工が可能です。</p>
				</div><!-- .facflex-last -->
				<div class="facflex-last clearfix">
					<h5 class="facflex-ltitle">
						<img src="<?php bloginfo('template_url'); ?>/img/content/facflex_step6.jpg" alt="工場紹介"/>
						<span>フィルム貼り</span>
					</h5>
					<p>PP・PETフィルム等を製品に貼ります。</p>
				</div><!-- .facflex-last -->
				<div class="facflex-last clearfix">
					<h5 class="facflex-ltitle">
						<img src="<?php bloginfo('template_url'); ?>/img/content/facflex_step7.jpg" alt="工場紹介"/>
						<span>用紙適正</span>
					</h5>
					<p>ロール状であれば紙、PET/PPシート以外にも12μフィルム等の軟包材やラベルなど様々な用紙に印刷可能です。</p>
				</div><!-- .facflex-last -->
				<div class="facflex-last clearfix">
					<h5 class="facflex-ltitle">
						<img src="<?php bloginfo('template_url'); ?>/img/content/facflex_step8.jpg" alt="工場紹介"/>
						<span>ラベル</span>
					</h5>
					<p>タック紙、スポット糊など接着剤の加工もできます。</p>
				</div><!-- .facflex-last -->
			</div><!-- #factory-content -->		
		</div><!-- #flexographic -->
		

		<div id="screen" class="mt40">		
			<h3 class="h3-title">スクリーン印刷</h3>
			<p class="mb30">美粧性へのこだわりをもってスクリーン印刷の可能性を求め続けます！</p>		
			
			<p class="facscreen-top"><img src="<?php bloginfo('template_url'); ?>/img/content/facscreen_top_img.jpg" alt="工場紹介" /></p>
			
			<div id="factory-content">
				<div class="factory-point1 first-child clearfix">
					<div class="fac-point1-left">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_point1_img1.jpg" alt="工場紹介" />
						<span>再現性</span>
					</div>
					<div class="fac-point1-right">あらゆる素材に印刷することができ、表現の幅も広いのが特徴ですが、生産スピードが低いため大ロットの場合、納品に時間がかかります。</div>
				</div><!-- .factory-point1 -->
				<div class="factory-point1 clearfix">
					<div class="fac-point1-left">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_point1_img2.jpg" alt="工場紹介" />
						<span>美粧性</span>
					</div>
					<div class="fac-point1-right">インクの厚盛りが可能なため、マットや疑似エンボスなど触れた時に触感に訴えかけることができます。</div>
				</div><!-- .factory-point1 -->
				<div class="factory-point1 clearfix">
					<div class="fac-point1-left">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_point1_img3.jpg" alt="工場紹介" />
						<span>コスト</span>
					</div>
					<div class="fac-point1-right">切り替え時間が早く、小ロットに対応しやすいのが特徴です。反面、大ロットにはコストがかかります。</div>
				</div><!-- .factory-point1 -->
				<div class="factory-point1 clearfix">
					<div class="fac-point1-left">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_point1_img4.jpg" alt="工場紹介" />
						<span>エコ</span>
					</div>
					<div class="fac-point1-right">小ロット生産でもヤレ紙がほとんど出ません。（ヤレ紙＝印刷機の調整等の過程で消費する紙）</div>
				</div><!-- .factory-point1 -->
			</div><!-- #factory-content -->
			
			<div id="factory-content">
				<ul class="facscreen-point2 clearfix">				
					<li>版材に絹（シルク）の布を使った印刷方式のため、シルク印刷と呼ばれています。</li>
					<li>近年はポリエステルや耐久性を考え金属性の材料で作られている版を使うことが多いのでスクリーン印刷と呼ばれることもあります。</li>
				</ul>				
			</div><!-- #factory-content -->
			
			<div id="factory-content">
				<h4 class="factory-subtitle">シルクスクリーン印刷で表現可能な加工</h4>
				<p class="mb30">
					弊社では、グラビア印刷（グラビアパール）、ホットスタンプ、エンボス、シルクスクリーン印刷など、枚葉印刷が可能な二次加工を組み合わせることができます。<br />
					※一部加工は外部の協力会社に依頼しています。
				</p>
				
				<div class="message-group message-col230 clearfix"><!-- begin message-group -->
					<div class="message-row clearfix"><!--message-row -->
						<div class="message-col">                
							<p><img src="<?php bloginfo('template_url'); ?>/img/content/facscreen_content_img1.jpg" alt="工場紹介"/></p>
							<h5 class="factory-ctitle">文字</h5>					        
						</div><!-- end message-col -->	
						<div class="message-col">                
							<p><img src="<?php bloginfo('template_url'); ?>/img/content/facscreen_content_img2.jpg" alt="工場紹介"/></p>
							<h5 class="factory-ctitle">厚盛り</h5>
						</div><!-- end message-col -->
						<div class="message-col">                
							<p><img src="<?php bloginfo('template_url'); ?>/img/content/facscreen_content_img3.jpg" alt="工場紹介"/></p>
							<h5 class="factory-ctitle">高細線</h5>
						</div><!-- end message-col -->
					</div><!-- end message-row -->
					
					<div class="message-row clearfix"><!--message-row -->
						<div class="message-col mt20">                
							<p><img src="<?php bloginfo('template_url'); ?>/img/content/facscreen_content_img4.jpg" alt="工場紹介"/></p>
							<h5 class="factory-ctitle">メタシャイン</h5>
						</div><!-- end message-col -->	
						<div class="message-col mt20">                
							<p><img src="<?php bloginfo('template_url'); ?>/img/content/facscreen_content_img5.jpg" alt="工場紹介"/></p>
							<h5 class="factory-ctitle">リオトーン</h5>
						</div><!-- end message-col -->					
					</div><!-- end message-row -->
				</div><!-- end message-group -->
			</div><!-- #factory-content -->
			
			<div id="factory-content">
				<h4 class="factory-subtitle">版の構造</h4>
				<p class="mb30">ポリエステルの糸で編まれた（網戸の様に）版を、絵柄部分以外に乳剤を塗布し網目を塞ぎます。※１乳剤を何回塗るかで版の厚みを設定し、厚盛りが出来ます、また１インチの中に何本の糸が有るかでベタ・文字等の版を設定します。</p>
			</div><!-- #factory-content -->
			
			<div id="factory-content" class="mb40">
				<h4 class="factory-subtitle">シルク印刷の特徴</h4>
				<p class="mb30">弊社ではクリア、マット、盛り上げ、リオトーン、メタシャインなどオフセット（枚葉印刷）の二次加工として使われています。</p>
				<div class="facscreen-last clearfix">
					<div class="facscreen-last-left">
						<h5 class="facscreen-ltitle">メリット</h5>
						<ul class="facscreen-ltext">
							<li>あらゆる素材に印刷可能</li>
							<li>厚盛りが可能</li>
							<li>小ロットに対応しやすい</li>
						</ul>
					</div>
					<div class="facscreen-last-right">
						<h5 class="facscreen-rtitle">デメリット</h5>
						<ul class="facscreen-rtext">
							<li>あらゆる素材に印刷可能</li>
							<li>厚盛りが可能</li>
							<li>小ロットに対応しやすい</li>
						</ul>
					</div>
				</div>
				<p class="pl20 pt20">※油性インキ（自然乾燥）とUVインキがあり、仕様・用紙等で決定します。</p>
			</div><!-- #factory-content -->	
		</div><!-- #screen -->
			
	</div><!-- end primary-row -->
	<?php get_template_part('part','flow_contact');?>
	
<?php get_footer(); ?>
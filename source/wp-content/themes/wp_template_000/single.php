<?php get_header();?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="subpages-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/blog_top_img.jpg" alt="スタッフブログ" />
			<span>スタッフブログ</span>
		</h2> 
		<p class="ln18em">制作実績やお客様のお言葉などを更新していきます。</p>		
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix">
		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
		<h2 class="h2-title"><?php the_title(); ?></h2>
		<div class="post-row-content clearfix">
			<!--<div class="post-row-meta">
				<i class="fa fa-clock-o"></i><?php the_time('l, F jS, Y'); ?>
				<i class="fa fa-tags"></i><?php the_category(' , ', get_the_id()); ?>
				<i class="fa fa-user"></i><span style="color:red;"><?php the_author_link(); ?></span>
			</div> -->
				
			<div class="blog-content"><?php the_content(); ?></div>
		</div><!-- .post-row-content -->   
		<?php endwhile; endif; ?>
		   
		<div class="navigation">
			<?php if( get_previous_post() ): ?>
			<div style="float:left;"><?php previous_post_link('%link', '« %title'); ?></div>
			<?php endif;
			if( get_next_post() ): ?>
			<div style="float:right;"><?php next_post_link('%link', '%title »'); ?></div>
			<?php endif; ?>
		</div>
		<!-- .navigation -->	
	</div><!-- end primary-row -->

	<?php get_template_part('part','flow_contact') ;?>

<?php get_footer();?>
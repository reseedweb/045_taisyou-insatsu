<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
   <p><img src="<?php bloginfo('template_url'); ?>/img/top/top_content_img.jpg" alt="<?php bloginfo('name');?>" /></p>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">化粧品パッケージ製作.comが選ばれる<span class="h2title-number">3</span><span class="h2title-clr">つの理由</span></h2> 
    <div class="message-group message-col230 clearfix"><!-- begin message-group -->
        <div class="message-row clearfix"><!--message-row -->
            <div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/top/top_box_img1.jpg" alt="<?php bloginfo('name');?>" />
                </div><!-- end image --> 
				<div class="point">
                    <img src="<?php bloginfo('template_url'); ?>/img/top/top_point1.jpg" alt="<?php bloginfo('name');?>" />
                </div><!-- end image -->				
				<h3 class="title">自社一貫生産</h3>
                <div class="text">
					<p>『化粧品パッケージ製作.com』ではパッケージ製作のほぼ全ての工程を自社工場内で行い全数検査及び混入対策を講じた品質管理を徹底し、品質を気にされるお客様にも満足いただいております。</p>
                </div>            
            </div><!-- end message-col -->
           
			<div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/top/top_box_img2.jpg" alt="<?php bloginfo('name');?>" />
                </div><!-- end image --> 
				<div class="point">
                    <img src="<?php bloginfo('template_url'); ?>/img/top/top_point2.jpg" alt="<?php bloginfo('name');?>" />
                </div><!-- end image -->				
				<h3 class="title">短納期対応可能</h3>
                <div class="text">
					<p>納期をお急ぎな案件に関しても、可能な限り短納期で対応いたします。1000個までの小ロットならデータ校了後最短５営業日での発送が可能です。お気軽にご相談ください。</p>
                </div>            
            </div><!-- end message-col -->
			
			<div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/top/top_box_img3.jpg" alt="<?php bloginfo('name');?>" />
                </div><!-- end image --> 
				<div class="point">
                    <img src="<?php bloginfo('template_url'); ?>/img/top/top_point3.jpg" alt="<?php bloginfo('name');?>" />
                </div><!-- end image -->				
				<h3 class="title">幅広い提案力</h3>
                <div class="text">
					<p>「作ってみたいパッケージがある」「予算感や仕様などの詳細が知りたい」など少しでも興味をお持ちの方は、企画段階でのご相談でも構いませんので、お問い合わせフォームやお電話からお気軽にご相談ください。</p>
                </div>            
            </div><!-- end message-col -->
        </div><!-- end message-row -->
    </div><!-- end message-group -->
	<p class="top-box-botttom"><img src="<?php bloginfo('template_url'); ?>/img/top/top_box_bottom.jpg" alt="<?php bloginfo('name');?>" /></p>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">初めてご利用される方へ</h2> 
	<div class="top-content-intro clearfix">
		<h3 class="top-conintro-title">化粧品パッケージ製作.comを<br />初めてご利用される方へ！</h3>
		<p class="top-conintro-text">化粧品パッケージ製作.comは化粧品パッケージ製作の専門会社です。<br />初めてご利用されるお客様をご案内致します！</p>
		<ul class="top-conintro-list">
			<li>安定した高品質ってホント？</li>
			<li>急ぎの案件だけど間に合う？</li>
			<li>繊細な色味にしたいんだけど？</li>
			<li>どうやって選んだらいいの？</li>
		</ul>
		<div class="top-conintro-btn"><a href="<?php bloginfo('url'); ?>/about"><img src="<?php bloginfo('template_url'); ?>/img/top/top_intro_btn.jpg" alt="<?php bloginfo('name');?>" /></a></div>
	</div><!-- .top-content-intro -->
	
	<div class="top-content-work clearfix">
		<h3 class="top-conwork-title">化粧品パッケージ製作.comの<br />自社工場をご紹介！</h3>
		<p class="top-conwork-text">化粧品パッケージ製作.comが保有する自社生産工<br />場の様子と機械設備をご紹介いたします！</p>		
		<div class="top-conwork-btn"><a href="<?php bloginfo('url'); ?>/factory"><img src="<?php bloginfo('template_url'); ?>/img/top/top_work_btn.jpg" alt="<?php bloginfo('name');?>" /></a></div>
	</div><!-- .top-content-work -->	
	<p class="mt20"><img src="<?php bloginfo('template_url'); ?>/img/top/top_box_bottom.jpg" alt="<?php bloginfo('name');?>" /></p>
</div><!-- end primary-row -->   

<?php get_template_part('part','flow_contact');?>

<div class="primary-row clearfix"><!-- begin primary-row -->
    <h2 class="h2-title">代表者からのメッセージ</h2>    
    <p class="ln18em">はじめまして、本サイトをご覧いただきありがとうございます。弊社は創業から81年間、化粧品パッケージの製作を通して「お客様のご要望を叶える」ことをモットーにお客様と共に歩んでまいりました。<br />
	近年IT化が進む中、販促業界の盛り上がりと共に化粧品パッケージ製作の需要も変わってまいりました。<br />
	そのような需要の中残念なことに、価格が安いだけで質や対応が悪かったり、要望と違うものが完成品として送られてきたりといった問題のある業者も少なくありません。<br />
	その点、弊社では全数検査及び混入対策を講じた品質管理を徹底し、高品質・丁寧な対応で、「月間50万個の制作実績」と「自社工場を保有」しているからこそできる強みがあります。<br />
	化粧品パッケージを製作されたいという方は、ぜひ「化粧品パッケージ製作.com」にお気軽にご相談ください。</p>
</div><!-- end primary-row -->
<?php get_footer(); ?>
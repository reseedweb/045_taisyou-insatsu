<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="subpages-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/factory_top_img.jpg" alt="工場紹介" />
			<span>工場紹介</span>
		</h2> 
				
		<p class="ln18em">
			弊社ではパッケージの制作に特化した工場設備を自社で保有しております。<br />
			様々な加工に対応できる機械を各複数用意していますので、多様な要望に短納期でお応えすることができます。<br />
			化粧品パッケージはパッケージを見た人が思わず手に取りたくなるようなデザインとそれを表現する印刷技術が求められます。オフセット印刷・フレキソ印刷・スクリーン印刷の３つの印刷方法を組み合わせることで、デザイナーの皆様が求める品質のパッケージをご提供します。
		</p>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="factory-tabs htabs clearfix">
			<a href="<?php bloginfo('url'); ?>/factory01#01" class="offset">オフセット印刷<i class="fa fa-arrow-down"></i></a>
			<a href="#flexographic" class="flexographic selected">フレキソ印刷<i class="fa fa-arrow-down"></i></a>
			<a href="#screen" class="screen">スクリーン印刷<i class="fa fa-arrow-down"></i></a>										
		</div>	
	</div><!-- end primary-row -->
	
	<div id="flexographic" class="primary-row clearfix">
		<h3 class="h3-title">フレキソ印刷</h3>
		<p class="mb30">エコロジー・スピーディ・ハイ・クオリティでフレキソ印刷＋αへの挑戦！</p>		
		
		<div class="videoWrapper">
			<iframe src="https://www.youtube.com/embed/9vjFdPf1Gmw?rel=0" frameborder="0" allowfullscreen></iframe>
		</div>
		
		<div id="factory-content">
			<div class="factory-point1 first-child clearfix">
				<div class="fac-point1-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/factory_point1_img1.jpg" alt="工場紹介" />
					<span>再現性</span>
				</div>
				<div class="fac-point1-right">水を使わない為、色ムラが発生しにくく、極小文字まで再現が可能で、特に白・墨ベタの印刷はキレイに再現されます。</div>
			</div><!-- .factory-point1 -->
			<div class="factory-point1 clearfix">
				<div class="fac-point1-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/factory_point1_img2.jpg" alt="工場紹介" />
					<span>美粧性</span>
				</div>
				<div class="fac-point1-right">コールドフォイルを使えば、箔の上からさらに印刷をすることができ、表現の幅を広げることができます。</div>
			</div><!-- .factory-point1 -->
			<div class="factory-point1 clearfix">
				<div class="fac-point1-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/factory_point1_img3.jpg" alt="工場紹介" />
					<span>コスト</span>
				</div>
				<div class="fac-point1-right">PET・PPシートはオフセット印刷に比べて原材料費を安く調達することができます。</div>
			</div><!-- .factory-point1 -->
			<div class="factory-point1 clearfix">
				<div class="fac-point1-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/factory_point1_img4.jpg" alt="工場紹介" />
					<span>エコ</span>
				</div>
				<div class="fac-point1-right">印刷資材にアルコール・有機溶剤を使用しておりませんので、環境負荷を軽減できます。</div>
			</div><!-- .factory-point1 -->
		</div><!-- #factory-content -->
		
		<div id="factory-content">
			<ul class="facflex-point2 clearfix">				
				<li>版は樹脂版で弾力のある版を使用します。</li>
				<li>印刷方式は凸版印刷で一般的な凸印刷に比べ印圧が軽くキスタッチと呼ばれています。</li>
				<li>アニロックスロールという細かいメッシュの彫刻ロールを使うことでインキを均一に転移させます。<br />
					※アニロックスロールはオフセット印刷のインキつぼの役割を果たし、ロールを変更するだけでインキ量を変えられます。同じインキを使用しても違う印刷色の表現が可能です。
				</li>
				<li>インキは溶剤系のインキではなくUVインキを使用し、また水を使わないため、環境にも優しい印刷方法です。</li>
			</ul>				
		</div><!-- #factory-content -->
		
		<div id="factory-content">
			<h4 class="factory-subtitle">フレキソ印刷で表現可能な加工</h4>
			<p class="mb30">
				弊社では、グラビア印刷（グラビアパール）、ホットスタンプ、エンボス、シルクスクリーン印刷など、枚葉印刷が可能な二次加工を組み合わせることができます。<br />
				※一部加工は外部の協力会社に依頼しています。
			</p>
			
			<div class="message-group message-col230 clearfix"><!-- begin message-group -->
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col">                
						<p><img src="<?php bloginfo('template_url'); ?>/img/content/facflex_content_img1.jpg" alt="工場紹介"/></p>
						<h5 class="factory-ctitle">クリアケース</h5>					        
					</div><!-- end message-col -->	
					<div class="message-col">                
						<p><img src="<?php bloginfo('template_url'); ?>/img/content/facflex_content_img2.jpg" alt="工場紹介"/></p>
						<h5 class="factory-ctitle">パール</h5>
					</div><!-- end message-col -->
					<div class="message-col">                
						<p><img src="<?php bloginfo('template_url'); ?>/img/content/facflex_content_img3.jpg" alt="工場紹介"/></p>
						<h5 class="factory-ctitle">擬似エンボス</h5>
					</div><!-- end message-col -->
				</div><!-- end message-row -->
				
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col mt20">                
						<p><img src="<?php bloginfo('template_url'); ?>/img/content/facflex_content_img4.jpg" alt="工場紹介"/></p>
						<h5 class="factory-ctitle">LC</h5>
					</div><!-- end message-col -->	
					<div class="message-col mt20">                
						<p><img src="<?php bloginfo('template_url'); ?>/img/content/facflex_content_img5.jpg" alt="工場紹介"/></p>
						<h5 class="factory-ctitle">ラベル</h5>
					</div><!-- end message-col -->	
					<div class="message-col mt20">                
						<p><img src="<?php bloginfo('template_url'); ?>/img/content/facflex_content_img6.jpg" alt="工場紹介"/></p>
						<h5 class="factory-ctitle">軟包材</h5>
					</div><!-- end message-col -->		
				</div><!-- end message-row -->
				
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col mt20">                
						<p><img src="<?php bloginfo('template_url'); ?>/img/content/facflex_content_img7.jpg" alt="工場紹介"/></p>
						<h5 class="factory-ctitle">クリアファイル</h5>
					</div><!-- end message-col -->	
				</div><!-- end message-row -->
			</div><!-- end message-group -->
		</div><!-- #factory-content -->
		
		<div id="factory-content" class="mb40">
			<h4 class="facflex-subtitle">弊社のフレキソ印刷機の特徴</h4>
			<div class="facflex-last clearfix">
				<h5 class="facflex-ltitle">
					<img src="<?php bloginfo('template_url'); ?>/img/content/facflex_step1.jpg" alt="工場紹介"/>
					<span>粒子の大きなインキ</span>
				</h5>
				<p>シルバー・パール・メタシャイン（ガラス）等の印刷ができます。</p>
			</div><!-- .facflex-last -->
			<div class="facflex-last clearfix">
				<h5 class="facflex-ltitle">
					<img src="<?php bloginfo('template_url'); ?>/img/content/facflex_step2.jpg" alt="工場紹介"/>
					<span>コールドファイル</span>
				</h5>
				<p>印刷用の版を使って、ほぼ印刷と同じ絵柄・文字等が再現できます。また、箔上に着色が可能なため、特注箔を作る必要がありません。</p>
			</div><!-- .facflex-last -->
			<div class="facflex-last clearfix">
				<h5 class="facflex-ltitle">
					<img src="<?php bloginfo('template_url'); ?>/img/content/facflex_step3.jpg" alt="工場紹介"/>
					<span>LCコート</span>
				</h5>
				<p>ホロフィルム等の柄を、製品に転写します。（柄は問い合わせください）</p>
			</div><!-- .facflex-last -->
			<div class="facflex-last clearfix">
				<h5 class="facflex-ltitle">
					<img src="<?php bloginfo('template_url'); ?>/img/content/facflex_step4.jpg" alt="工場紹介"/>
					<span>反転バー</span>
				</h5>
				<p>用紙を反転させ、両面同時印刷が可能です。</p>
			</div><!-- .facflex-last -->
			<div class="facflex-last clearfix">
				<h5 class="facflex-ltitle">
					<img src="<?php bloginfo('template_url'); ?>/img/content/facflex_step5.jpg" alt="工場紹介"/>
					<span>擬似エンボス</span>
				</h5>
				<p>UVコート等のコーティングが出来るので、疑似加工が可能です。</p>
			</div><!-- .facflex-last -->
			<div class="facflex-last clearfix">
				<h5 class="facflex-ltitle">
					<img src="<?php bloginfo('template_url'); ?>/img/content/facflex_step6.jpg" alt="工場紹介"/>
					<span>フィルム貼り</span>
				</h5>
				<p>PP・PETフィルム等を製品に貼ります。</p>
			</div><!-- .facflex-last -->
			<div class="facflex-last clearfix">
				<h5 class="facflex-ltitle">
					<img src="<?php bloginfo('template_url'); ?>/img/content/facflex_step7.jpg" alt="工場紹介"/>
					<span>用紙適正</span>
				</h5>
				<p>ロール状であれば紙、PET/PPシート以外にも12μフィルム等の軟包材やラベルなど様々な用紙に印刷可能です。</p>
			</div><!-- .facflex-last -->
			<div class="facflex-last clearfix">
				<h5 class="facflex-ltitle">
					<img src="<?php bloginfo('template_url'); ?>/img/content/facflex_step8.jpg" alt="工場紹介"/>
					<span>ラベル</span>
				</h5>
				<p>タック紙、スポット糊など接着剤の加工もできます。</p>
			</div><!-- .facflex-last -->
		</div><!-- #factory-content -->		
	</div><!-- #flexographic -->
	
	
	<?php get_template_part('part','flow_contact');?>
	
<?php get_footer(); ?>
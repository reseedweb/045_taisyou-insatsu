<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="subpages-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_top_img.jpg" alt="ご注文の流れ" />
			<span>ご注文の流れ</span>
		</h2> 
		<p class="ln18em">こちらでは、お客様がお問い合わせしてから、商品がお手元に届くまでの流れを紹介しております。お問い合わせやお見積もりの前に、ぜひ一度ご確認くださいませ。</p>		
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">1.お問い合わせ</h3>
		<div class="message-left message-308 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img1.jpg" alt="<?php bloginfo('name');?>" />
			</div>
			<div class="text ln18em">
				<p>まずはお気軽にお問い合わせください。<br />
				その際、化粧品パッケージのある程度仕様を決めて頂けると、打ち合わせがスムーズです。<br />
				もし、パッケージの仕様のことで迷っている項目や疑問点がございましたら、お気軽にお尋ねください。<br />
				価格のご相談や、パッケージ以外のデザイン制作物のご依頼もお待ちしております。<br />
				基本的に即日見積りを目指しています。<br />
				パッケージの仕様によってはお見積りに時間をいただく場合もございます。ご了承ください。</p>
			</div>
		</div><!-- end message-308 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">2.お見積り</h3>
		<div class="message-left message-308 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img2.jpg" alt="<?php bloginfo('name');?>" />
			</div>
			<div class="text ln18em">
				<p>打ち合わせ内容でお聞きしたご要望より、化粧品パッケージのお見積りをお伝えさせていただきます。仕様を迷われている場合は、複数案のお見積りも可能ですので、お気軽にご相談ください。</p>
			</div>
		</div><!-- end message-308 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">3.サンプル制作</h3>
		<div class="message-left message-308 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img3.jpg" alt="<?php bloginfo('name');?>" />
			</div>
			<div class="text ln18em">
				<p>化粧品パッケージのサンプル品を作成いたします。<br />
					実際に内容物を入れていただき、サイズ・形状・仕様などに問題ないかご確認いただいてから量産に入ります。サイズなどの最終調整をし、化粧品パッケージの仕様を決定いたします。<br />
					サンプル品は量産前提であれば、無料でお作りいただけます。<br />
					サンプル制作後キャンセルになる場合は、サンプル制作費のみご請求させていただきます。</p>
			</div>
		</div><!-- end message-308 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">4.ご注文</h3>
		<div class="message-left message-308 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img4.jpg" alt="<?php bloginfo('name');?>" />
			</div>
			<div class="text ln18em">
				<p>サンプル制作後、決定した仕様に基づいての見積りをご提出します。仕様、見積りに納得いただきましたら、正式にご注文となります。</p>
			</div>
		</div><!-- end message-308 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">5.デザイン制作・データ入稿</h3>
		<div class="message-left message-308 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img5.jpg" alt="<?php bloginfo('name');?>" />
			</div>
			<div class="text ln18em">
				<p>パッケージのデザインデータをご入稿ください。<br />データ形式はAdobe IllustratorのCCまでを推奨しております。<br />Adobe Illustrator以外のデータは別途費用かかる場合がございますので、ご了承ください。</p>
			</div>
		</div><!-- end message-308 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">6.箱製造</h3>
		<div class="message-left message-308 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img6.jpg" alt="<?php bloginfo('name');?>" />
			</div>
			<div class="text ln18em">
				<p>「化粧品パッケージ製作.com」運営の自社工場により箱の製造を行います。製造された箱は丁寧に検品が行われ出荷されます。化粧品パッケージ製作の過程につきましては、こちらのページで詳しく掲載しております。よろしければ、ご覧下さい。</p>
			</div>
		</div><!-- end message-308 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">7.納品</h3>
		<div class="message-left message-308 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img7.jpg" alt="<?php bloginfo('name');?>" />
			</div>
			<div class="text ln18em">
				<p>全国のお客様へ発送、指定日にあわせて納品いたします。</p>
			</div>
		</div><!-- end message-308 -->
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','flow_contact');?>	
<?php get_footer(); ?>
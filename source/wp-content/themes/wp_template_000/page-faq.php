<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="subpages-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/faq_top_img.jpg" alt="よくあるご質問" />
			<span>よくあるご質問</span>
		</h2> 
		<p class="ln18em">化粧品パッケージ製作に関してよくある質問を掲載いたします。<br />こちらに掲載されている以外のご質問は直接お問い合わせください。</p>		
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="faq-content-question">パッケージとはどんなものですか？</h3>
		<div class="faq-content-answer ln18em">
			<img src="<?php bloginfo('template_url'); ?>/img/content/faq_icon_answer.jpg" alt="よくあるご質問" class="faq-icon-answer"/>
			<p>化粧品、衣料品、食料品や日用品、ギフトなどを、商品にあわせてデザインされた箱で包装し、ディスプレイ効果などの機能を兼ね備えた箱がパッケージと呼ばれます。商品のイメージに合わせて、紙、クリアケースなどの素材を選ぶことができます。</p>			
		</div><!-- .faq-content-answer -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="faq-content-question">オリジナルパッケージを作りたいのですが、手順がわかりません</h3>
		<div class="faq-content-answer ln18em">
			<img src="<?php bloginfo('template_url'); ?>/img/content/faq_icon_answer.jpg" alt="よくあるご質問" class="faq-icon-answer"/>
			<p>まずはお電話か、当サイトのお問い合わせフォームよりご要望をお聞かせください。お客様のニーズに合わせて使用する材料やご予算に応じて設計いたします。「はじめての方へ」や「ご注文の流れ」ページにて手順などご案内しております。そちらも参考にご覧ください。</p>			
		</div><!-- .faq-content-answer -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="faq-content-question">用紙のサンプルや、印刷物のサンプルなど送ってもらえますか？</h3>
		<div class="faq-content-answer ln18em">
			<img src="<?php bloginfo('template_url'); ?>/img/content/faq_icon_answer.jpg" alt="よくあるご質問" class="faq-icon-answer"/>
			<p>もちろん可能です。仕様にお悩みの際は、紙素材のサンプルなどご用意させていただきますので、お気軽にお問い合わせください。 ※取り寄せの必要な特殊紙や、メール便を越えるサイズの大きい物は、有償な場合もございます。予めご了承ください。</p>			
		</div><!-- .faq-content-answer -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="faq-content-question">見本・サンプル品は作ってもらえますか？</h3>
		<div class="faq-content-answer ln18em">
			<img src="<?php bloginfo('template_url'); ?>/img/content/faq_icon_answer.jpg" alt="よくあるご質問" class="faq-icon-answer"/>
			<p>はい。弊社では基本的にサンプル制作を行い、実際に中身を入れて使用感をご確認いただいてから、量産に入らせて頂いております。</p>			
		</div><!-- .faq-content-answer -->
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','flow_contact');?>	
<?php get_footer(); ?>
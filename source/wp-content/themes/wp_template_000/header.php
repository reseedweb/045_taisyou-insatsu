<!DOCTYPE html>
<html>
    <head>
        <!-- meta -->        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />         
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- title -->
        <title><?php wp_title( '|', true); ?></title>
        <meta name="robots" content="noindex,follow,noodp" />
        
        <link rel="profile" href="http://gmpg.org/xfn/11" />                
        
        <!-- global javascript variable -->
        <script type="text/javascript">
            var CONTAINER_WIDTH = '1090px';
            var CONTENT_WIDTH = '1060px';
            var BASE_URL = '<?php bloginfo('url'); ?>';
            var TEMPLATE_URI = '<?php bloginfo('template_url') ?>';
            var CURRENT_MODULE_URI = '';
            Date.now = Date.now || function() { return +new Date; };            
        </script>        
        
        <!-- fontawesome -->
        <link href="<?php bloginfo('template_url'); ?>/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
        <![endif]-->        
        
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.plugins.js" type="text/javascript"></script>               
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.reseed.js" type="text/javascript"></script>		
		<script src="<?php bloginfo('template_url'); ?>/js/tgImageChangeV2.js" type="text/javascript"></script>		
		<script src="<?php bloginfo('template_url'); ?>/js/config.js" type="text/javascript"></script>        
		<link href="<?php bloginfo('template_url'); ?>/style.css?<?php echo md5(date('l jS \of F Y h:i:s A')); ?>" rel="stylesheet" />        
					
		<?php if(is_page('factory')) : ?>																				
			<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
			<link href="<?php bloginfo('template_url'); ?>/css/jquery-ui.css" rel="stylesheet" />        
			<link href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" />		
		<?php endif; ?>
        				
        <?php wp_head(); ?>
    </head>
    <body>     
        <div id="screen_type"></div>        
        <div id="wrapper"><!-- begin wrapper -->		
            <section id="top"><!-- begin top -->                
                <div class="contain clearfix">
                    <h1 class="top-text">化粧品パッケージ・印刷・製作なら化粧品パッケージ製作.com</h1>
                    <ul class="top-navi">
                        <li><a href="<?php bloginfo('url'); ?>/about"><i class="fa fa-arrow-right"></i>はじめての方へ</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/faq"><i class="fa fa-arrow-right"></i>よくあるご質問</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/company"><i class="fa fa-arrow-right"></i>会社概要</a></li>                        
                    </ul>					
                </div>                
            </section><!-- end top -->            

            <header><!-- begin header -->
                <div class="contain clearfix">
                    <div class="header-content clearfix">            
                        <div class="logo">
                            <a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/img/common/logo.jpg" alt="<?php bloginfo('name');?>" /></a>
                        </div>
                        <div class="header-tel">
                            <img src="<?php bloginfo('template_url'); ?>/img/common/header_tel.jpg" alt="<?php bloginfo('name');?>" /></a>
                        </div>
						<div class="header-con">
                            <a href="<?php bloginfo('url'); ?>/contact"><img src="<?php bloginfo('template_url'); ?>/img/common/header_con.jpg" alt="<?php bloginfo('name');?>" /></a>
                        </div>
                    </div>                
                </div>
            </header><!-- end header -->
            
			<?php get_template_part('part','navi'); ?>

            <?php if(is_home()) : ?>
                <?php get_template_part('part','banner'); ?>
				<?php get_template_part('part','topmain'); ?>
            <?php else : ?>            
				<?php get_template_part('part','breadcrumb'); ?>
			<?php endif;?>	
			
            <section id="content"><!-- begin content -->
                <div class="contain two-cols-left clearfix"><!-- begin two-cols -->
                    <main class="primary"><!-- begin primary -->
       
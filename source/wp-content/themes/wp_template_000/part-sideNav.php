	<h2 class="side-title">ご利用ガイド</h2>
    <ul class="sideNav">
        <li><a href="<?php bloginfo('url'); ?>/estimation">見積りを依頼する<i class="fa fa-chevron-right"></i></a></li>
        <li><a href="<?php bloginfo('url'); ?>/about">はじめての方へ<i class="fa fa-chevron-right"></i></a></li>
        <li><a href="<?php bloginfo('url'); ?>/flow">ご注文の流れ<i class="fa fa-chevron-right"></i></a></li>
        <li><a href="<?php bloginfo('url'); ?>/faq">よくあるご質問<i class="fa fa-chevron-right"></i></a></li>
        <li><a href="<?php bloginfo('url'); ?>/draft">データ入稿について<i class="fa fa-chevron-right"></i></a></li>
        <li><a href="<?php bloginfo('url'); ?>/factory">工場紹介<i class="fa fa-chevron-right"></i></a></li>
        <li><a href="<?php bloginfo('url'); ?>/company">会社案内<i class="fa fa-chevron-right"></i></a></li>
		<li><a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー<i class="fa fa-chevron-right"></i></a></li>
		<li><a href="<?php bloginfo('url'); ?>/contact">お問い合わせ<i class="fa fa-chevron-right"></i></a></li>
    </ul>

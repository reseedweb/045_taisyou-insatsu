<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="subpages-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/display_top_img.jpg" alt="加飾方法" />
			<span>加飾方法</span>
		</h2> 
				
		<p class="ln18em">
			化粧品パッケージを手に取ってもらうために、表現に大きな選択肢を与える「加飾」。<br />
			印刷された後、様々な方法で加飾を施されたパッケージは、見ている人の感性にダイレクトに訴求します。<br />
			こちらのページではパッケージに施す加飾の種類についてご紹介します。 
		</p>
	</div><!-- end primary-row -->

	
	<div id="01" class="primary-row clearfix">
		<h3 class="h3-title">ホットスタンプ</h3>
		<p class="mb25 ln18em">印刷ではできないような金属光沢が得られます。箔表面の接着層が、加熱された版に圧力が加わることで紙に転写されます。金属光沢感がありますので、ロゴなどのエンブレムの印刷に最適です。</p>
		<div class="message-group message-col230 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img1.jpg" alt="加飾方法"/></p>
					<p class="pt15">スタンダードな金銀転写箔は艶・消など用途により様々なタイプがあります。またカラーバリエーションも豊富ですので多様化するデザインにも対応できます。</p>
				</div><!-- end message-col -->	
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img2.jpg" alt="加飾方法"/></p>
					<p class="pt15">ホログラム箔は、光り方の特徴からデザインの付加価値をUPさせます。</p>
				</div><!-- end message-col -->
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img3.jpg" alt="加飾方法"/></p>
					<p class="pt15">スタンダードな艶シルバー箔です。高級感を出す際によく使われます。</p>
				</div><!-- end message-col -->
			</div><!-- end message-row -->		
		</div><!-- end message-group -->		
	</div><!-- #display01 -->
		
		
	<div id="02" class="primary-row clearfix">
		<h3 class="h3-title">コールドフォイル</h3>
		<p class="mb25 ln18em">ホットスタンプ同様に容易に金属光沢が得られますが、フォイル加工の上にカラーや特色印刷 を行うことができる新しい表現技術です。網点を使ったグラデーションや細文字などの繊細なデザイン、箔のベタ印刷などデザインの幅 が広がります。</p>
		<div class="message-group message-col230 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img4.jpg" alt="加飾方法"/></p>
					<p class="pt15">コールドフォイルは金・銀・ホログラム等の箔がありますが、箔（銀）上に印刷または着色することでバリエーションを増やすことも可能です。</p>
				</div><!-- end message-col -->	
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img5.jpg" alt="加飾方法"/></p>
					<p class="pt15">特注箔を使用する必要がありませんので、コストダウンにもつながります。</p>
				</div><!-- end message-col -->
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img6.jpg" alt="加飾方法"/></p>
					<p class="pt15">通常のホットスタンプでは再現しづらい細かな線状の柄も可能です。</p>
				</div><!-- end message-col -->
			</div><!-- end message-row -->		
		</div><!-- end message-group -->		
	</div><!-- #display02 -->
	
	
	<div id="03" class="primary-row clearfix">
		<h3 class="h3-title">エンボス</h3>
		<p class="mb25 ln18em">紙に凹凸感を表現する加工で、凹ませたり浮き出しで表現します。紙だけでデザインできる加工としてパッケージにアクセントをつけます。また、ホットスタンプと組み合わせることも可能なので、ロゴマークを強調したりワンポイントのデザインを凹ませたりと多様に広がります。また、ホットプレス（空押し)で色調が変わるような紙（例：OKフロートなど）との相性も◎です。</p>
		<div class="message-group message-col230 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img7.jpg" alt="加飾方法"/></p>
					<p class="pt15">インクを使わず紙に模様を入れたり手触りを良くしたり、見た目と手触りとの二重の効果を与えることが出来ます。ホットスタンプ同様にロゴなどのエンブレムに最適です。</p>
				</div><!-- end message-col -->	
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img8.jpg" alt="加飾方法"/></p>
					<p class="pt15">カチコミは凹ませるタイプのエンボスです。</p>
				</div><!-- end message-col -->
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img9.jpg" alt="加飾方法"/></p>
					<p class="pt15">凸タイプのエンボスは浮き出しと呼んでいます。</p>
				</div><!-- end message-col -->
			</div><!-- end message-row -->		
		</div><!-- end message-group -->		
	</div><!-- #display03 -->
	
	
	<div id="04" class="primary-row clearfix">
		<h3 class="h3-title">LCコート</h3>
		<p class="mb25 ln18em">
			UV硬化型樹脂の表面に微細なホログラムPPの凹凸をつけることにより、ホログラムを転写する表面加工です。<br />
			スポット表現ができ、グロス、マット、ホログラム等の表面加工が可能です。
		</p>
		<div class="message-group message-col230 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img10.jpg" alt="加飾方法"/></p>
					<p class="pt15">フィルム貼りよりは高価になりますが、印刷面に表情を出しますので高級感の演出など他製品と差をつけたい時に有効な加工です。</p>
				</div><!-- end message-col -->	
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img11.jpg" alt="加飾方法"/></p>
					<p class="pt15">掲載写真以外にも柄は豊富にあります。</p>
				</div><!-- end message-col -->
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img12.jpg" alt="加飾方法"/></p>
					<p class="pt15">
						手の油分などで柄が消えることがあります。（注意が必要です。）<br />
						※細かな凹凸に油分が埋まるだけなので、ふき取れば柄は復活します。
					</p>
				</div><!-- end message-col -->
			</div><!-- end message-row -->		
		</div><!-- end message-group -->		
	</div><!-- #display04 -->
	
	
	<div id="05" class="primary-row clearfix">
		<h3 class="h3-title">フィルム貼り</h3>
		<p class="mb25 ln18em">
			PET（ポリエステル）やPP（ポリプロピレン）フィルムに接着剤を塗布し、熱圧着で加工する表面加工です。<br />
			表面に光沢を得られ、強度も高く耐水性や耐摩擦性もあり、高温・多湿の状態 にしてもブロッキングが起きにくいのが利点です。
		</p>
		<div class="message-group message-col230 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img13.jpg" alt="加飾方法"/></p>
					<p class="pt15">表面加工の中でも製函適性や耐摩性が高く、その分コストも上がりますが印刷面に光沢を与えます。またマット調のフィルムもあり、PET・PPを使い分けることも可能です。</p>
				</div><!-- end message-col -->	
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img14.jpg" alt="加飾方法"/></p>
					<p class="pt15">光沢のあるフィルムは透明性が高く、マットフィルムは落ち着いた印象を与えます。</p>
				</div><!-- end message-col -->
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img15.jpg" alt="加飾方法"/></p>
					<p class="pt15">ベース紙により、平滑の違いがかなり出ます。</p>
				</div><!-- end message-col -->
			</div><!-- end message-row -->		
		</div><!-- end message-group -->		
	</div><!-- #display05 -->
	
	
	<div id="06" class="primary-row clearfix">
		<h3 class="h3-title">プレスコート</h3>
		<p class="mb25 ln18em">印刷物の表面に熱で固まり光沢を出す熱硬化樹脂を印刷表面に塗って乾燥させた後、ローラーでプレスします。ビニール引きに比べ光沢や耐摩擦性が高いのが特徴です。 エンドレスプレスが主流となっています。また、ベトナム工場でもプレスコート加工が可能です。</p>
		<div class="message-group message-col230 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img16.jpg" alt="加飾方法"/></p>
					<p class="pt15">フィルム貼りに比べ、耐摩強度や耐水性は劣りますが、比較的安価に光沢を得られることができます。</p>
				</div><!-- end message-col -->	
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img17.jpg" alt="加飾方法"/></p>
					<p class="pt15">ラミネート（フィルム貼り）に近い光沢を得られます。</p>
				</div><!-- end message-col -->
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img18.jpg" alt="加飾方法"/></p>
					<p class="pt15">原紙表面の凹凸を埋める役割もありますので、平滑性のある表面に仕上がります。</p>
				</div><!-- end message-col -->
			</div><!-- end message-row -->		
		</div><!-- end message-group -->		
	</div><!-- #display06 -->
	
	
	<div id="07" class="primary-row clearfix">
		<h3 class="h3-title">UVコート</h3>
		<p class="mb25 ln18em">
			UVコートとはニスコートの一種で、紫外線を照射させることで乾燥・硬化させる表面加工です。<br />
			プレスコートと同様の光沢感が得られ、耐摩性もアップします。仕上がりの質感は光沢とマットがあります。
		</p>
		<div class="message-group message-col230 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img19.jpg" alt="加飾方法"/></p>
					<p class="pt15">表面加工の中でも優れた耐摩性を持ち、透明性も高く印刷面の色合いを損なうことがないため、デザイン等の訴求力が落ちることはありません。</p>
				</div><!-- end message-col -->	
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img20.jpg" alt="加飾方法"/></p>
					<p class="pt15">光沢・平滑性があがるツヤのUVコートです。</p>
				</div><!-- end message-col -->
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img21.jpg" alt="加飾方法"/></p>
					<p class="pt15">弊社では、ＰＥＴ蒸着紙用のＵＶコートもあります。</p>
				</div><!-- end message-col -->
			</div><!-- end message-row -->		
		</div><!-- end message-group -->		
	</div><!-- #display07 -->
	
	
	<div id="08" class="primary-row clearfix">
		<h3 class="h3-title">ニス・ビニール引き</h3>
		<p class="mb25 ln18em">印刷面に透明な樹脂液または塩化ビニール系合成樹脂等を塗り、表面をコーティングすることをニス引き・ビニール引きといいます。 表面加工の中では比較的安価な加工で、インクの色落ちを防ぐことを目的としています。</p>
		<div class="message-group message-col230 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img22.jpg" alt="加飾方法"/></p>
					<p class="pt15">風合いはグロス感やマット調の他に、耐摩・耐熱・耐油・耐水などのブリスターコートなどもあり、必要な機能に合わせて様々な加工を施すことが可能です。</p>
				</div><!-- end message-col -->	
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img23.jpg" alt="加飾方法"/></p>
					<p class="pt15">表面をニス引きやビニール引きすることで、印刷色により若干黄味が増すことがあります。</p>
				</div><!-- end message-col -->
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img24.jpg" alt="加飾方法"/></p>
					<p class="pt15">マット調のニス引きもあります。</p>
				</div><!-- end message-col -->
			</div><!-- end message-row -->		
		</div><!-- end message-group -->		
	</div><!-- #display08 -->
	
	
	<div id="09" class="primary-row clearfix">
		<h3 class="h3-title">ロールエンボス</h3>
		<p class="mb25 ln18em">ロールエンボスは、原紙に溶剤等を直接塗布するような表面加工ではありませんが、印刷デザインやパッケージの質感をグレードアップさせることを目的とした加工です。掲載写真以外にも、エンボス柄は多数あります。</p>
		<div class="message-group message-col230 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img25.jpg" alt="加飾方法"/></p>
					<p class="pt15">ロールエンボスは印刷後に施す加工になりますので、エンボスの底の部分にインクが乗っていない、などという心配は必要ありません。</p>
				</div><!-- end message-col -->	
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img26.jpg" alt="加飾方法"/></p>
					<p class="pt15">逆に、最初からエンボスが施されている紙を使用することも可能です。この場合には印刷に注意が必要です。</p>
				</div><!-- end message-col -->
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img27.jpg" alt="加飾方法"/></p>
					<p class="pt15">掲載写真以外にも、エンボス柄は多数あります。</p>
				</div><!-- end message-col -->
			</div><!-- end message-row -->		
		</div><!-- end message-group -->		
	</div><!-- #display09 -->
	
	
	<div id="10" class="primary-row clearfix">
		<h3 class="h3-title">UVハジキ印刷</h3>
		<p class="mb25 ln18em">印刷とコーティング加工がワンパスで実現します。低価格で高クオリティーな、疑似エンボスとも呼ばれる浮き出し効果のある印刷です。</p>
		<div class="message-group message-col230 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img28.jpg" alt="加飾方法"/></p>
					<p class="pt15">部分的にはじかせることも可能です。その際、高価な樹脂版は必要ありません。</p>
				</div><!-- end message-col -->	
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img29.jpg" alt="加飾方法"/></p>
					<p class="pt15">キズ防止や指紋付着軽減にも効果があります。</p>
				</div><!-- end message-col -->
				
				<div class="message-col">                
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/display_content_img30.jpg" alt="加飾方法"/></p>
					<p class="pt15">白板紙・蒸着紙・PET・PPなど様々な用紙に印刷可能です。</p>
				</div><!-- end message-col -->
			</div><!-- end message-row -->		
		</div><!-- end message-group -->		
	</div><!-- #display10 -->
		
<?php get_footer(); ?>
<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="subpages-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/company_top_img.jpg" alt="会社概要" />
			<span>会社概要</span>
		</h2> 
		<p class="ln18em">下記注意書きをお読みになった上で、お手持ちのデザインデータをご入稿ください。</p>		
	</div><!-- end primary-row -->
		
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">会社概要</h3>
		<table class="company-content">
			<tr>
				<th>会社名</th>
				<td>大昇印刷株式会社</td>
			</tr>
			<tr>
				<th>所在地</th>
				<td>
					【本社】〒578-0983／大阪府東大阪市吉田下島12番15号<br />
					【東京営業所】〒104-0061／東京都中央区銀座3丁目7番13号成田屋ビル5階
					【併合会社】Los 26-28, Tan Tao St., Tan Tao industrial Park, Tan Tao A ward, Binh Tan district, HCMC
				</td>
			</tr>
			<tr>
				<th>TEL</th>
				<td>【本社】072-962-5301（代）【東京営業所】03-3564-5168</td>
			</tr>
			<tr>
				<th>FAX</th>
				<td>【本社】072-964-2621【東京営業所】03-3564-6858</td>
			</tr>
			<tr>
				<th>創業</th>
				<td>昭和9年3月1日</td>
			</tr>
			<tr>
				<th>資本金</th>
				<td>1,000万円</td>
			</tr>
			<tr>
				<th>従業員数</th>
				<td>89名（2015年4月1日）</td>
			</tr>
			<tr>
				<th>役員</th>
				<td>
					代表取締役社長　松本裕士<br />
					代表取締役副社長　松本耕一
				</td>
			</tr>
			<tr>
				<th>主要取引先</th>
				<td>
					花王株式会社・株式会社カネボウ化粧品・株式会社ノエビア・<br />
					株式会社ファンケル美健・その他
				</td>
			</tr>
		</table>
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">会社沿革</h3>
		<table class="company-content">
			<tr>
				<th>1934年（昭和9年）3月</th>
				<td>
					大阪市東区内久宝寺2丁目9にて<br />
					松本保一が松本印刷所創業	
				</td>
			</tr>
			<tr>
				<th>1949年（昭和24年）1月</th>
				<td>
					資本金400万円をもって法人に改組<br />
					大昇印刷株式会社を設立<br />
					代表取締役に松本保一就任
				</td>
			</tr>
			<tr>
				<th>1964年（昭和39年）4月</th>
				<td>東京墨田に東京営業所設立</td>
			</tr>
			<tr>
				<th>1965年（昭和40年）2月</th>
				<td>吉田下島12-15に1,000坪取得<br />
					本社・工場を移転		
				</td>
			</tr>
			<tr>
				<th>1982年（昭和57年）4月</th>
				<td>‘82ジャパンパッケージングコンペティションにて日本印刷工業会長賞受賞</td>
			</tr>
			<tr>
				<th>1983年（昭和58年）4月</th>
				<td>東京・銀座に東京営業所移転</td>
			</tr>
			<tr>
				<th>1994年（平成 6年）4月</th>
				<td>‘94ジャパンパッケージングコンペティションにて化粧品部門賞受賞</td>
			</tr>
			<tr>
				<th>2003年（平成15年）12月</th>
				<td>松本裕士 代表取締役に就任</td>
			</tr>
			<tr>
				<th>2005年（平成17年）5月</th>
				<td>ISO9001 認証取得</td>
			</tr>
			<tr>
				<th>2007年（平成19年）8月</th>
				<td>物流センター竣工</td>
			</tr>
			<tr>
				<th>2009年（平成21年）6月</th>
				<td>コンビネーション印刷機導入</td>
			</tr>
			<tr>
				<th>2012年（平成24年）6月</th>
				<td>ベトナム・ホーチミン市に子会社Taisho & 7　Packaging　Technology 設立</td>
			</tr>	
			<tr>
				<th>2014年（平成26年）5月</th>
				<td>ハイデルベルグ社　オフセット6色コーター機導入</td>
			</tr>	
		</table>
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">機械設備一覧</h3>
		<table class="company-content">
			<tr>
				<th>ハイデルベルグ社製</th>
				<td>
					スピードマスターCD102Z-2色機<br />
					スピードマスター6色機<br />
					スピードマスターCP2000-4色機<br />
					PZ自動箔押機<br />
					シリンダー自動箔押機<br />
				</td>
			</tr>
			<tr>
				<th>ギーツ社製</th>
				<td>平圧式自動箔押機</td>
			</tr>
			<tr>
				<th>池田箔押研究所製</th>
				<td>反自動箔押機</td>
			</tr>
			<tr>
				<th>大日本スクリーン社製</th>
				<td>
					イメージセッターFT-P5055<br />
					CTP PT-R8000II<br />
					ULTRA CUBE 701G焼付
				</td>
			</tr>
			<tr>
				<th>フジフィルム製造社製</th>
				<td>
					PS800ES現像機<br />
					PS9000VR現像機
				</td>
			</tr>			
			<tr>
				<th>菅野製作所社製</th>
				<td>
					NF-1050ニューオートン自動打抜機<br />
					GM-450サックマシン<br />
					GM-650サックマシン
				</td>
			</tr>
			<tr>
				<th>サクライスクリーン社製</th>
				<td>SC102Aシルクスクリーン機</td>
			</tr>
			<tr>
				<th>勝田製作所製</th>
				<td>
					JMC-III2000裁断機<br />
					他
				</td>
			</tr>
		</table>
	</div><!-- end primary-row -->
	
		
	<div class="primary-row clearfix">
		<h3 class="h3-title">アクセス</h3>		
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3281.2364768586235!2d135.6185358!3d34.67398070000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x600120679839d0ed%3A0xad59718a51cb77e3!2s12-15+Yoshitashimojima%2C+Higashi%C5%8Dsaka-shi%2C+%C5%8Csaka-fu+578-0983%2C+Japan!5e0!3m2!1sen!2s!4v1442472537197" width="750" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
<?php get_footer(); ?>
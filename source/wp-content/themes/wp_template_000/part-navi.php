			<nav>
                <div class="contain clearfix">
					<ul class="navi-list">
						<li>
							<a href="<?php bloginfo('url'); ?>/">
								<span class="first-text">トップページ</span>
								<span class="second-text">toppage</span>   
							</a>
						</li>
						<li>
							<a href="<?php bloginfo('url'); ?>/#lineup">
								<span class="first-text">ラインナップ</span>
								<span class="second-text">line up</span>   
							</a>
						</li>
						<li>
							<a href="<?php bloginfo('url'); ?>/price">
								<span class="first-text">参考価格</span>
								<span class="second-text">price</span>   
							</a>
						</li>
						<li>
							<a href="<?php bloginfo('url'); ?>/flow">
								<span class="first-text">ご注文の流れ</span>
								<span class="second-text">flow</span>   
							</a>
						</li>
						<li>
							<a href="<?php bloginfo('url'); ?>/draft">
								<span class="first-text">データ入稿</span>
								<span class="second-text">draft</span>   
							</a>
						</li>
						<li>
							<a href="<?php bloginfo('url'); ?>/estimation">
								<span class="first-text">見積りを依頼する</span>
								<span class="second-text">estimation</span>   
							</a>
						</li>
					</ul><!-- end header-info2 -->
				</div>
            </nav>
<?php get_header(); ?>
	<div class="primary-row clearfix">
		<h2 class="h2-title">お問い合わせ</h2>
		<div class="contact-form">			
			<?php echo do_shortcode('[contact-form-7 id="42" title="お問い合わせ"]') ?>						
		</div>
	</div>
	<?php get_template_part('part','flow_contact'); ?>
	<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>	
	<script type = 'text/javascript'> 
		$( document ).ready(function() {
			$('#zip').change(function(){					
				//AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
	    		AjaxZip3.zip2addr(this,'','pref','addr1','addr2');
	  		});
		});
	</script>
<?php get_footer(); ?>
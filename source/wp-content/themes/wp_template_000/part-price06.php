	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">窓貼り</h3>
		<table class="price-table">
			<tr>
				<th>ロット数</th>
				<th>１色単価例</th>
				<th>4色単価例</th>
				<th>6色単価例</th>
			</tr>
			<tr>
				<th>1,000</th>
				<td>100円</td>
				<td>158円</td>				
				<td>120円</td>
			</tr>
			<tr>
				<th>5,000</th>
				<td>29円</td>
				<td>37円</td>				
				<td>26円</td>
			</tr>
			<tr>
				<th>10,000</th>
				<td>20円</td>
				<td>23円</td>				
				<td>15円</td>
			</tr>
		</table><!-- .price-table -->
	</div><!-- end primary-row -->
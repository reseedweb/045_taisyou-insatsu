<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="subpages-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/draft_top_img.jpg" alt="データ入稿" />
			<span>データ入稿</span>
		</h2> 
		<p class="ln18em">下記注意書きをお読みになった上で、お手持ちのデザインデータをご入稿ください。</p>		
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">完全データでのご入稿をお願いしております</h3>
		<p class="ln18em">
			「完全データ」とは、修正の必要がない、完成された印刷可能な制作データのことです。<br />
			データ入稿の前には、下記注意事項の入念なチェックをお願いいたします。<br />
			なお、データに不備があった場合、弊社で入稿データの修正をいたします。<br />
			結果、ご入稿日が変わってまいります。<br />
			※受付日や出荷予定日も同様に変わりますので、最初のご入稿日から計算される日にちとは異なります。<br /?>
			あらかじめご了承ください。<br />
			お客様で入稿データを作成するのが難しい場合は、弊社でイメージのデータ化やデザイン制作も承っております。（別途費用）お気軽にご相談ください。
		</p>
	
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">データ入稿での注意事項（必読）</h3>
		<div class="draft-content clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">デザインの原稿は基本的にIllustrator(ai/eps)形式でお送りください。<br />バージョンはCCまで対応しております。</p>
		</div>
		
		<div class="draft-content clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">Illustratorをお持ちでない方に限って、PDFのデータでお送りください。</p>
		</div>
		
		<div class="draft-content clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">恐れ入りますが、Word、Excel形式のデータはお受けすることが出来かねます。</p>
		</div>
		
		<div class="draft-content clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">カラーモードはCMYKを選択してください。</p>
		</div>
		
		<div class="draft-content clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">解像度は350dpi以上にしてください。解像度が低いと画像が荒くなる可能性があります。</p>
		</div>
		
		<div class="draft-content clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">Illustratorに配置した画像は全て「リンク」にしてください。</p>
		</div>
		
		<div class="draft-content clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">フォントは必ず「文字のアウトライン化」をしてください。</p>
		</div>
		
		<div class="draft-content clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">データの作成は、原寸にてお願いします。</p>
		</div>
		
		<div class="draft-content clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">添付ファイルはできるだけ圧縮して、容量は最大7MBくらいにして頂けるようにお願いします。</p>
		</div>
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">データ入稿はこちらから</h3>
		<p class="ln18em">デザインデータを入稿の際は担当者にご連絡ください。<br />入稿前に上記注意書きを必ずお読み頂くようにお願いいたします。</p>
		<div class="draft-content-last clearfix">
			<p class="draft-info-first">
				<span class="draft-first-title">※容量の大きいファイルの場合</span><br />
				<span class="draft-icon">■</span>7MBを超える容量の大きいデータは極力データを圧縮いただくか、下記データ転送サービスをご利用ください。<br />
				・宅ファイル便（300MBまで無料） <a href="http://www.filesend.to/" target="_blank">http://www.filesend.to/</a><br />
				・ギガファイル便（容量無制限） <a href="http://www.gigafile.nu/" target="_blank">http://www.gigafile.nu/</a><br />
				<span class="draft-icon">■</span>デザインデータをCD-ROMもしくはUSBなどに収容し、下記宛先まで郵送でお送りいただく事も可能です。 尚お送りいただいたCD-ROMなどのご返却出来かねますので、ご了承ください。
			</p>
			<p class="draft-info-last">
				<span class="draft-last-title">※データ送付先</span><br />
				〒578-0983 大阪府東大阪市吉田下島12番15号<br />
				大昇印刷株式会社　化粧品パッケージ製作.com宛
			</p>
		</div>
	</div><!-- end primary-row -->
	
	
	<?php get_template_part('part','flow_contact');?>	
<?php get_footer(); ?>
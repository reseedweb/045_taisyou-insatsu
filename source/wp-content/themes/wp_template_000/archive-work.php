<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="subpages-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/work_top_img.jpg" alt="制作実績" />
			<span>制作実績</span>
		</h2> 
		<p class="ln18em">化粧品パッケージ製作.comでご依頼いただいた制作実績をご紹介いたします。</p>		
	</div><!-- end primary-row -->

	<?php
	    $work_posts = get_posts( 	array(
			'post_type'=> 'work',
			'posts_per_page' => 3,
			'paged' => get_query_var('paged'),
		));
	?>
	<?php
		$i = 0;	    	    	    
	    foreach($work_posts as $work_post) :  
	?>
	<div class="primary-row clearfix">
		<h3 class="h3-title"><?php echo $work_post->post_title; ?></h3>	
		<div class="work-content clearfix">
			<div class="work-left">
				<?php echo get_the_post_thumbnail($work_post->ID,'medium'); ?>
			</div>
			<table class="work-right">
				<tr>
					<th>デザイン</th>
					<td><?php echo get_field('text1', $work_post->ID); ?></td>
				</tr>
				<tr>
					<th>印刷技術</th>
					<td><?php echo get_field('text2', $work_post->ID); ?></td>
				</tr>
				<tr>
					<th>特長</th>
					<td><?php echo get_field('text3', $work_post->ID); ?></td>
				</tr>				
			</table>
		</div><!-- end message-308 -->		
		<?php $value = get_field( "text4", $work_post->ID);
		if( $value ) {
			echo '<div class="work-info-last">';														
			echo $value;
			echo '</div>';
		} ?>
	</div>
	<?php endforeach;?>
	<div class="primary-row text-center">
		<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	</div>
	<?php wp_reset_query(); ?>
<?php get_footer(); ?>